#!/usr/bin/env bash

set -euo pipefail

# Default mode
DRY_RUN=false

command_exists() {
  command -v "$1" >/dev/null 2>&1
}

run_command() {
  if [ "$DRY_RUN" = true ]; then
    echo "[DRY RUN] Would execute: $*"
  else
    "$@"
  fi
}

install_homebrew() {
  if ! command_exists curl; then
    echo "curl is required to install Homebrew. Please install curl first."
    exit 1
  fi
  echo "Installing Homebrew..."
  if [ "$DRY_RUN" = true ]; then
    echo "[DRY RUN] Would execute: /bin/bash -c \"$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)\""
  else
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  fi
}

install_docker() {
  if command_exists docker; then
    echo "Docker is already installed."
  else
    if [[ "$OSTYPE" == "darwin"* ]]; then
      echo "Installing Docker for macOS..."
      if ! command_exists brew; then
        echo "Homebrew not found. Installing..."
        install_homebrew
      else
        echo "Homebrew is already installed."
      fi
      run_command brew install --cask docker
    elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
      if command_exists apt-get; then
        echo "Installing Docker for Debian/Ubuntu..."
        run_command sudo apt-get update
        run_command sudo apt-get install -y docker.io
      elif command_exists yum; then
        echo "Installing Docker for CentOS/RHEL..."
        run_command sudo yum install -y docker
      elif command_exists dnf; then
        echo "Installing Docker for Fedora..."
        run_command sudo dnf install -y docker
      else
        echo "Unsupported Linux distribution. Please install Docker manually."
        exit 1
      fi
    else
      echo "Unsupported operating system."
      exit 1
    fi
  fi
}

install_colima() {
  if command_exists colima; then
    echo "Colima is already installed."
  else
    if [[ "$OSTYPE" == "darwin"* ]]; then
      echo "Installing Colima..."
      run_command brew install colima
    elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
      echo "Downloading Colima..."
      if command_exists curl; then
        run_command curl -LO https://github.com/abiosoft/colima/releases/latest/download/colima-"$(uname)"-"$(uname -m)"
        run_command chmod +x "colima-$(uname)-$(uname -m)"
        run_command sudo install "colima-$(uname)-$(uname -m)" /usr/local/bin/colima
      else
        echo "curl is required to install Colima. Please install curl and run the script again."
        echo "Alternatively, visit https://github.com/abiosoft/colima for manual installation instructions."
        exit 1
      fi
    else
      echo "Unsupported operating system."
      exit 1
    fi
  fi
}

set_docker_context() {
  echo "Starting Colima..."
  run_command colima start

  echo "Setting Docker context to use Colima..."
  run_command docker context use colima
}

pull_docker_image() {
  echo "Pulling base docker image..."
  run_command docker pull registry.gitlab.com/gitlab-org/duo-workflow/default-docker-image/workflow-generic-image:v0.0.4
  echo "Image pulled successfully"
}

modify_vscode_settings() {
  local key="gitlab.duoWorkflow.dockerSocket"
  local value="${HOME}/.colima/default/docker.sock"
  local settings_file

  # Determine the path to the VS Code settings file based on the OS
  if [[ "$OSTYPE" == "darwin"* ]]; then
    settings_file="$HOME/Library/Application Support/Code/User/settings.json"
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    settings_file="$HOME/.config/Code/User/settings.json"
  else
    echo "Unsupported operating system for VS Code settings modification."
    return 1
  fi

  # Ensure the settings file exists
  if [ ! -f "$settings_file" ]; then
    echo "{}" >"$settings_file"
  fi

  # Modify the settings file
  if [ "$DRY_RUN" = true ]; then
    echo "[DRY RUN] Would modify VS Code settings: $key = $value"
  else
    # Read the entire file
    content=$(<"$settings_file")

    # Remove trailing comma if exists
    content=${content%,}

    # Remove the closing brace
    content=${content%\}}

    # Add new key-value pair
    if [ -z "$content" ] || [ "$content" = "{" ]; then
      # File is empty or only contains opening brace
      new_content="{\"$key\": \"$value\"}"
    else
      # File has existing content
      new_content="$content, \"$key\": \"$value\"}"
    fi

    # Write the modified content back to the file
    echo "$new_content" >"$settings_file"

    echo "VS Code setting updated: $key = $value"
  fi
}

# Parse command line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
  --dry-run) DRY_RUN=true ;;
  *)
    echo "Unknown parameter passed: $1"
    exit 1
    ;;
  esac
  shift
done

# Main script
echo "Script running in $(if [ "$DRY_RUN" = true ]; then echo "DRY RUN"; else echo "NORMAL"; fi) mode"

if ! command_exists curl; then
  echo "curl is required for this script. Please install curl and run the script again."
  exit 1
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "Detected macOS environment..."
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
  echo "Detected Linux environment."
else
  echo "Unsupported operating system for Duo Workflow pre-requisites installation. Please install the dependencies manually."
  exit 1
fi

install_docker
install_colima
set_docker_context
pull_docker_image
modify_vscode_settings

echo "Setup complete."
