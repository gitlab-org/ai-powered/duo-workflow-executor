package sentry

import (
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
)

// Config contains configuration for sentry
type Config struct {
	DSN              string
	Environment      string
	TelemetryEnabled bool
	Logger           *zap.Logger
	Debug            bool
	Tags             map[string]string
}

// ConfigureSentry configures the sentry DSN
func ConfigureSentry(version string, sentryConf Config) {
	if !sentryConf.TelemetryEnabled {
		sentryConf.Logger.Info("Telemetry is disabled, skipping Sentry initialization...")
		return
	}
	if sentryConf.DSN == "" {
		sentryConf.Logger.Warn("Sentry DSN is empty, skipping Sentry initialization...")
		return
	}
	// Sentry requires Flush to be called before terminating the program to avoid
	// unintentionally dropping events. os.exit(1) bypasses defer functions. To avoid
	// calling flush after each CaptureException call, we have SDK send events over the network synchronously
	// https://pkg.go.dev/github.com/getsentry/sentry-go#Client.Flush
	sentrySyncTransport := sentry.NewHTTPSyncTransport()
	sentrySyncTransport.Timeout = time.Second * 3

	err := sentry.Init(sentry.ClientOptions{
		Dsn:            sentryConf.DSN,
		Transport:      sentrySyncTransport,
		Environment:    sentryConf.Environment,
		Release:        "v" + version,
		BeforeSend:     removePrivateInfoFields,
		MaxBreadcrumbs: 0,
		Debug:          sentryConf.Debug,
		Tags:           sentryConf.Tags,
	})
	if err != nil {
		sentryConf.Logger.Error("unable to initialize sentry client")
		return
	}

	sentryConf.Logger.Debug("Using sentry for error logging....")
}

func removePrivateInfoFields(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
	updatedEvent := event

	if updatedEvent.ServerName != "" {
		updatedEvent.ServerName = ""
	}
	if updatedEvent.Contexts != nil {
		delete(updatedEvent.Contexts, "culture")
		delete(updatedEvent.Contexts, "device")
		delete(updatedEvent.Contexts, "app")
	}
	return updatedEvent
}
