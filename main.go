package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"strings"
	"syscall"

	sentry "github.com/getsentry/sentry-go"
	sentryconfig "gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/config/sentry"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/internal/services/runner"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/internal/services/tokenrevoker"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/client"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/git"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/retry"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/workflow"
	"gitlab.com/gitlab-org/labkit/correlation"
	"go.uber.org/zap"
)

var version string

const (
	exitCodeSuccess                    = 0
	exitCodeUnknownError               = 1
	exitCodeLoggerCreationFailed       = 2
	exitCodeTokenRevokerCreationFailed = 3
	exitCodeClientCreationFailed       = 4
	exitCodeExecutionFailed            = 5
)

type workflowConfig struct {
	id, goal, definition, metadata string
}

type gitlabConfig struct {
	baseURL, token, realm, instanceID string
}

type workflowServiceConfig struct {
	addr, token string
}

type bootConfig struct {
	userID, basePath, stubsDir, logfile, clientCapabilities, namespaceID string
	debug, recordStubs, insecure, telemetryEnabled                       bool
	workflow                                                             workflowConfig
	git                                                                  git.Config
	gitlab                                                               gitlabConfig
	workflowService                                                      workflowServiceConfig
}

func main() {
	boot := &bootConfig{}

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintf(flag.CommandLine.Output(), "Exit codes:\n")
		printExitCodeHelp(exitCodeLoggerCreationFailed, "Unable to create logger")
		printExitCodeHelp(exitCodeTokenRevokerCreationFailed, "Unable to create token revoker")
		printExitCodeHelp(exitCodeExecutionFailed, "Unable to create client")
		printExitCodeHelp(exitCodeExecutionFailed, "Execution failed to run")
	}

	flag.StringVar(&boot.workflowService.addr, "server", os.Getenv("DUO_WORKFLOW_SERVICE_SERVER"), "The server address in the format of host:port")
	flag.StringVar(&boot.workflowService.token, "duo-workflow-service-token", os.Getenv("DUO_WORKFLOW_SERVICE_TOKEN"), "JWT for Duo Service Workflow authentication")
	flag.StringVar(&boot.workflow.id, "workflow-id", os.Getenv("DUO_WORKFLOW_WORKFLOW_ID"), "The ID for the workflow")
	flag.StringVar(&boot.workflow.definition, "definition", os.Getenv("DUO_WORKFLOW_DEFINITION"), "The selected workflow definition file")
	flag.StringVar(&boot.workflow.metadata, "workflow-metadata", os.Getenv("DUO_WORKFLOW_METADATA"), "Additional metadata, in JSON format, that is passed through to Duo Workflow Service.")
	flag.StringVar(&boot.workflow.goal, "goal", os.Getenv("DUO_WORKFLOW_GOAL"), "The goal to be completed by the workflow")
	flag.StringVar(&boot.gitlab.baseURL, "base-url", os.Getenv("GITLAB_BASE_URL"), "The base URL of the gitlab instance")
	flag.StringVar(&boot.gitlab.token, "token", os.Getenv("GITLAB_OAUTH_TOKEN"), "The token for authentication")
	flag.StringVar(&boot.gitlab.realm, "realm", os.Getenv("DUO_WORKFLOW_SERVICE_REALM"), "Realm value to be sent with x-gitlab-realm header")
	flag.StringVar(&boot.gitlab.instanceID, "instance-id", os.Getenv("DUO_WORKFLOW_INSTANCE_ID"), "Instance id value to be sent with x-gitlab-instance-id header")
	flag.StringVar(&boot.git.HTTPBaseURL, "git-http-base-url", os.Getenv("DUO_WORKFLOW_GIT_HTTP_BASE_URL"), "The base url of the git http server.")
	flag.StringVar(&boot.git.HTTPSUser, "git-http-user", os.Getenv("DUO_WORKFLOW_GIT_HTTP_USER"), "The user name for the Git.")
	flag.StringVar(&boot.git.Password, "git-password", os.Getenv("DUO_WORKFLOW_GIT_HTTP_PASSWORD"), "The password for the Git.")
	flag.StringVar(&boot.git.UserEmail, "git-user-email", "duo.workflow.agent@gitlab.com", "The user email for the Git.")
	flag.BoolVar(&boot.git.IgnoreDirOwners, "ignore-git-dir-owners", false, "Whether to ignore the owners of the Git directory.")
	flag.StringVar(&boot.basePath, "base-path", defaultBaseDir(), "The directory where the workflow will read/write files and run commands")
	flag.StringVar(&boot.userID, "user-id", os.Getenv("DUO_WORKFLOW_GLOBAL_USER_ID"), "Gitlab global user id")
	flag.StringVar(&boot.stubsDir, "stubs-dir", "", "Directory containing GitLab API call stubs. Useful only for testing purposes.")
	flag.StringVar(&boot.logfile, "logfile", "", "File used for storing logs, if not set STDOUT and STDERR is used.")
	flag.StringVar(&boot.clientCapabilities, "client-capabilities", "", "Comma separated list of capabilities supported by the client")
	flag.BoolVar(&boot.recordStubs, "record-stubs", false, "When using stubs (stubs-dir is set) and a stub is missing, then a real request is done and the response is stored for next use")
	flag.BoolVar(&boot.debug, "debug", os.Getenv("DUO_WORKFLOW_DEBUG") == "true", "Enable debugging information (e.g. debug logs)")
	flag.BoolVar(&boot.insecure, "insecure", os.Getenv("DUO_WORKFLOW_INSECURE") == "true", "Use when the Duo Workflow Service does not have TLS enabled (e.g. local development).")
	flag.BoolVar(&boot.telemetryEnabled, "telemetry-enabled", os.Getenv("DUO_WORKFLOW_TELEMETRY_ENABLED") == "true", "Enable telemetry (e.g. error tracking)")
	flag.StringVar(&boot.namespaceID, "namespace-id", os.Getenv("DUO_WORKFLOW_NAMESPACE_ID"), "The project's namespace id")

	flag.Parse()

	ctx := correlation.ContextWithCorrelation(context.Background(), correlation.SafeRandomID())

	logger, loggerDispose, err := boot.createLogger(ctx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create logger: %v\n", err)
		sentry.CaptureException(err)
		os.Exit(exitCodeLoggerCreationFailed)
	}
	defer loggerDispose()

	exitCode, err := boot.run(ctx, logger)
	if err != nil {
		logger.Error("Failed to run", zap.Error(err))
		sentry.CaptureException(err)
		os.Exit(exitCode)
	}
}

// Needed because language server does not set it and expects /workspace
func defaultBaseDir() string {
	fromEnv := os.Getenv("DUO_WORKFLOW_BASE_PATH")
	if fromEnv != "" {
		return fromEnv
	}
	return "/workspace"
}

func (boot *bootConfig) run(ctx context.Context, logger *zap.Logger) (_ int, err error) {
	// Capture a panic and return it as a general error to log and track
	defer func() {
		if recvErr := recover(); recvErr != nil {
			err = fmt.Errorf("panic handled: %v", recvErr)
		}
	}()

	sentryConf := sentryconfig.Config{
		DSN:              "https://a4d2369fba4cfa1ed8f0f63037c300c9@new-sentry.gitlab.net/40",
		Environment:      "production",
		TelemetryEnabled: boot.telemetryEnabled,
		Logger:           logger,
		Debug:            boot.debug,
		Tags:             map[string]string{"correlation_id": correlation.ExtractFromContext(ctx)},
	}

	sentryconfig.ConfigureSentry(version, sentryConf)

	tokenRevoker, err := tokenrevoker.NewTokenRevoker(boot.gitlab.baseURL, boot.gitlab.token)
	if err != nil {
		return exitCodeTokenRevokerCreationFailed, fmt.Errorf("failed to create token revoker: %v", err)
	}
	defer func() {
		if revokeErr := tokenRevoker.RevokeToken(ctx); revokeErr != nil {
			err = fmt.Errorf("could not revoke token: %v", revokeErr)
		}
	}()

	metadata := &client.ClientMetadata{
		GitlabBaseURL:           boot.gitlab.baseURL,
		GitlabToken:             boot.gitlab.token,
		DuoWorkflowServiceRealm: boot.gitlab.realm,
		InstanceID:              boot.gitlab.instanceID,
		DuoWorkflowServiceToken: boot.workflowService.token,
		DuoWorkflowGlobalUserID: boot.userID,
		WorkflowMetadata:        workflow.NewMetadata(boot.workflow.metadata, logger).WithGitInfo(boot.basePath),
		NamespaceID:             boot.namespaceID,
	}

	var clientCapabilitiesList []string
	if boot.clientCapabilities != "" {
		clientCapabilitiesList = strings.Split(boot.clientCapabilities, ",")
	}

	client, err := client.New(
		logger,
		boot.workflowService.addr,
		metadata,
		boot.insecure,
		clientCapabilitiesList,
	)
	if err != nil {
		return exitCodeClientCreationFailed, fmt.Errorf("failed to create client: %v", err)
	}
	defer client.Close()

	logger.Info("Starting client", zap.String("version", version), zap.String("server", boot.workflowService.addr))

	runnerConfig := &runner.RunnerConfig{
		Client:         client,
		Version:        version,
		Goal:           boot.workflow.goal,
		Definition:     boot.workflow.definition,
		BaseURL:        boot.gitlab.baseURL,
		Token:          boot.gitlab.token,
		BasePath:       boot.basePath,
		WorkflowID:     boot.workflow.id,
		StubsDir:       boot.stubsDir,
		GitConfig:      &boot.git,
		RecordStubs:    boot.recordStubs,
		Logger:         logger,
		AnalyticSender: actions.NewSendAnalyticsEvent(boot.gitlab.token, boot.gitlab.baseURL, boot.workflow.id),
	}

	executor := runner.NewExecutionRunner(runnerConfig)
	err = retry.WithRetry(ctx, executor, logger, nil)
	if err != nil {
		return exitCodeExecutionFailed, fmt.Errorf("failed to execute runner: %v", err)
	}

	return exitCodeSuccess, err
}

func (boot *bootConfig) createLogger(ctx context.Context) (*zap.Logger, func(), error) {
	var logConfig zap.Config

	if boot.debug {
		logConfig = zap.NewDevelopmentConfig()
	} else {
		logConfig = zap.NewProductionConfig()
	}

	if boot.logfile != "" {
		logConfig.OutputPaths = []string{boot.logfile}
		logConfig.ErrorOutputPaths = []string{boot.logfile}
	}

	logConfig.InitialFields = map[string]interface{}{
		"correlation_id": correlation.ExtractFromContext(ctx),
		"workflow_id":    boot.workflow.id,
	}

	logger, err := logConfig.Build()
	if err != nil {
		return nil, nil, err
	}
	zap.ReplaceGlobals(logger)

	return logger, func() {
		err = logger.Sync()
		if err != nil && !errors.Is(err, syscall.ENOTTY) {
			_, _ = fmt.Fprintf(os.Stderr, "failed to sync logger %s", err)
			return
		}
	}, nil
}

func printExitCodeHelp(exitCode int, meaning string) {
	fmt.Fprintf(flag.CommandLine.Output(), "  %d\n", exitCode)
	fmt.Fprintf(flag.CommandLine.Output(), "        %s\n", meaning)
}
