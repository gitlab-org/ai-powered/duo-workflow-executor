# Maintainership

**NOTE: Currently, this project is short-handed. Please consider becoming a maintainer!**

Duo Workflow Executor is currently a small project, so we can follow [the common guideline](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#maintainership-process-for-smaller-projects)
for a new reviewer, trainee maintainer and maintainer.
If you have a question, please reach out one of the maintainers that you can find in the [GitLab Review Workload Dashboard](https://gitlab-org.gitlab.io/gitlab-roulette/?currentProject=duo-workflow-executor).

## Preferred domain knowledge

To maintain the Duo Workflow Service project, the following domain knowledge is preferred:

- [Golang](https://go.dev) as the primary programming language.
- [Architectural blueprint](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/duo_workflow/) to understand how Duo Workflow is designed and how the executor integrates with the overall design.

## How to become a maintainer

While there is no strict guideline how to become a maintainer, we generally recommend the following activities before submitting the request:

- Author at least 5 MRs.
- Review at least 5 MRs as a trainee maintainer.
- Familiarize yourself with the [preferred domain knowledge](#preferred-domain-knowledge).

When it's ready:

- Create a merge request and indicate your role as an `duo-workflow-executor: maintainer` in your [team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md).
- Assign MR to your manager and Duo Workflow Executor maintainers for merge.
