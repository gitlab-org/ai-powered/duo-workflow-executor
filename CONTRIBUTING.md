
## Developer Certificate of Origin and License

All contributions are subject to the
[Developer Certificate of Origin and License](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin).

## Code of Conduct

[View the documentation](https://about.gitlab.com/community/contribute/code-of-conduct/) to find the latest information.

## Style guides

For code style, see [`Go standards and guidelines`](https://docs.gitlab.com/ee/development/go_guide/).

## Code Formatting

#### Formatting Tools

This project uses gofumpt for Go code formatting, which is a stricter version of gofmt.
gofumpt enforces additional rules for consistent code style beyond the standard formatting.

**Installing gofumpt**

`go install mvdan.cc/gofumpt@latest`

**Formatting Rules**

gofumpt applies all standard gofmt rules plus additional formatting rules including:
- Consistent ordering of declarations
- Grouped import blocks with consistent spacing
- Simplified struct, interface, and function declarations
- Consistent empty line placement
- Removal of redundant type declarations

##### IDE Integration

Most Go-compatible IDEs support gofumpt through plugins or direct integration:

**VS Code**: Use the official Go extension and set "go.formatTool": "gofumpt" in settings

**GoLand**: Install the File Watchers plugin and configure it to run gofumpt

**Vim/Neovim**: Configure gopls to use gofumpt or use ALE/similar plugins

For more information:  https://github.com/mvdan/gofumpt?tab=readme-ov-file#installation

## Commits

For commits style and guidelines, see [`Gitlab Commit messages guidelines`](https://docs.gitlab.com/development/contributing/merge_request_workflow/#commit-messages-guidelines).