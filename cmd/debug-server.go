package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/protobuf/encoding/protojson"

	service "gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb/contract"
)

type server struct {
	service.UnimplementedDuoWorkflowServer
}

func (s server) ExecuteWorkflow(srv service.DuoWorkflow_ExecuteWorkflowServer) error {
	log.Println("Got ExecuteWorkflow:")
	ctx := srv.Context()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		req, err := srv.Recv()
		if err == io.EOF {
			log.Println("exit")
			return nil
		}
		if err != nil {
			log.Printf("receive error %v", err)
			continue
		}

		response := req.GetResponse()

		log.Printf("%v", response)

		defaultAction := `{"requestID":"abc123","runCommand":{"command":"ls"}}`
		fmt.Print("Action to send (" + defaultAction + "):")
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		text = strings.TrimSpace(text)

		if text == "" {
			text = defaultAction
		}

		action := &service.Action{}
		_ = protojson.Unmarshal([]byte(text), action)
		_ = srv.Send(action)
	}
}

func main() {
	lis, err := net.Listen("tcp", ":50052")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	service.RegisterDuoWorkflowServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
