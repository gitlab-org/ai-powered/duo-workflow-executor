## What does this MR do and **why**?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

## Related issues

<!--
Add any related issues here.
-->


## Screenshots or screen recordings

_Screenshots are required for UI changes, and strongly recommended for all other merge requests._

<!--
Please include any relevant screenshots or screen recordings that will assist
reviewers and future readers. If you need help visually verifying the change,
please leave a comment and ping a GitLab reviewer, maintainer, or MR coach.
-->

| Before | After  |
| ------ | ------ |
|        |        |

## How to set up and validate locally

<!--
Numbered steps to set up and validate the change are strongly suggested

For example:
1. Configure GDK to use the branch for this MR:
   ```
   gdk config set duo_workflow.service_version <insert branch name>
   gdk reconfigure
   ```
1. <Include any steps required to set up the test environment. E.g., create issues/MRs/epics in GDK>
1. In VSCode, run Duo Workflow with a goal like `<insert goal relevant to the changes in the MR>`.
1. Confirm that Duo Workflow <insert description of expected behavior> 

For additional examples see:
- https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-executor/-/issues/37
- https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-executor/-/issues/31
- https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-executor/-/issues/27
- https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-executor/-/issues/29

-->

/assign me

/label ~"group::duo workflow" ~"Category:Duo Workflow" ~"devops::ai-powered" ~"section::data-science" 

<!--
Pick a merge request type label set.

/label ~"type::feature" ~"feature::addition"
/label ~"type::feature" ~"feature::consolidation"
/label ~"type::feature" ~"feature::enhancement"

/label ~"type::maintenance" ~"maintenance::dependency"
/label ~"type::maintenance" ~"maintenance::performance"
/label ~"type::maintenance" ~"maintenance::pipelines"
/label ~"type::maintenance" ~"maintenance::refactor"
/label ~"type::maintenance" ~"maintenance::release"
/label ~"type::maintenance" ~"maintenance::removal"
/label ~"type::maintenance" ~"maintenance::workflow"

/label ~"type::bug" ~"bug::functional"
/label ~"type::bug" ~"bug::performance"
/label ~"type::bug" ~"bug::vulnerability"
-->

<!--
Set the milestone.

Example: /milestone %17.5
-->
