# Duo Workflow Executor

The Duo Workflow Executor is a component of the Duo Workflow feature.

Learn how to use Duo Workflow by viewing the [documentation](https://docs.gitlab.com/ee/user/duo_workflow/).

Learn more about the technical implementation and vision for Duo Workflow by viewing the [architecture design document](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/duo_workflow/).

## Local development

Learn how to set up all the components for Duo Workflow in the [developer documentation](https://docs.gitlab.com/ee/development/duo_workflow/).

To specifically set up the Duo Workflow Executor for local development do the following:

### Installing runtime dependencies

To install require runtime dependencies for Duo Workflow, execute the below command:

```bash
./scripts/install-runtime
```

### Running the executor

For ease of use you can [set up Duo Workflow directly with GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/duo_workflow.md?ref_type=heads).

In general, you can run the executor either directly on your host machine or in a docker container.

#### On your host machine

##### With gRPC auth disabled

You need a GitLab PAT or an OAuth access token with scope `ai_workflows` which you need to create in your GitLab Instance.

In the Duo Workflow Executor terminal, execute the below command:

```bash
make && \
./bin/duo-workflow-executor \
  --goal='Fix the pipeline for the Merge request 62 in the project 19.' \
  --insecure --debug \
  --workflow-id=$WORKFLOW_ID \
  --token=$YOUR_GITLAB_PAT \
  --base-url="$GDK_GITLAB_URL" \
  --user-id="$GITLAB_ANONYMOUS_USER_ID"
  --namespace-id="$GITLAB_PROJECT_NAMESPACE_ID"
```

###### How to fetch namespace ID:

Run the following command with your Gitlab PAT:

```bash

curl -X GET "http://gdk.test:3000/api/v4/projects/${GITLAB_PROJECT_ID}" -H "Authorization: Bearer $YOUR_GITLAB_PAT" | jq

```

From the response, look for `namespace` object:

```json

  "namespace": {
    "id": 33,
    "name": "Flightjs",
    "path": "flightjs",
    "kind": "group",
    "full_path": "flightjs",
    "parent_id": null,
    "avatar_url": null,
    "web_url": "http://gdk.test:3000/groups/flightjs"
  },

```

##### With gRPC auth enabled

1. Make the direct_access API request and store the response

   ```bash
   response=$(curl -s --location `-request POST 'http://gdk.test:3000/api/v4/ai/duo_workflows/direct_access?private_token=$PAT' \
     --header 'Content-Type: application/json' \
     --data ''`
   # you can use OAuth access token instead of PAT in the above API
   ```

1. Extract values using [`jq`](https://jqlang.github.io/jq/), passing the response directly

   ```bash
   USER_ID=$(echo "$response" | jq -r '.duo_workflow_service.headers["X-Gitlab-Global-User-Id"]')
   WF_SERVICE_TOKEN=$(echo "$response" | jq -r '.duo_workflow_service.token')
   GITLAB_REALM=$(echo "$response" | jq -r '.duo_workflow_service.headers["X-Gitlab-Realm"]')
   GITLAB_INSTANCE_ID=$(echo "$response" | jq -r '.duo_workflow_service.headers["X-Gitlab-Instance-Id"]')
   WF_SERVICE_URL=$(echo "$response" | jq -r '.duo_workflow_service.base_url')
   ```

1. Call Duo Workflow service with the associated details

   ```bash
   make && ./bin/duo-workflow-executor \
     --token="$OAUTH_TOKEN" \
     --debug \  # optional
     --realm="$GTILAB_REALM" \
     --user-id="$USER_ID" \
     --instance-id=$GITLAB_INSTANCE_ID \  # optional
     --duo-workflow-service-token="$WF_SERVICE_TOKEN" \
     --base-url="http://gdk.test:3000" \
     --server="$WF_SERVICE_URL" \
     --goal="$WORKFLOW_GOAL" \
     --workflow-id=$WORKFLOW_ID \
     --namespace-id=$GITLAB_PROJECT_NAMESPACE_ID \ 
     --git-http-base-url="http://gdk.test:3000" \  # optional
     --git-http-user=$GIT_USERNAME \  # optional
     --git-password="$OAUTH_TOKEN" \  # optional
     --ignore-git-dir-owners  # optional
   ```

#### Run in Docker

1. Run `GOOS=linux GOARCH=amd64 make` to compile the binary
1. Run `mkdir workspace` to create a folder for the executor to work in
1. Start the docker container:

   ```bash
   docker run -it \
     --rm \
     --mount type=bind,source="$(pwd)/bin/duo-workflow-executor",target=/tmp/duo-workflow-executor \
     -v "$(pwd)/workspace":/workspace registry.gitlab.com/gitlab-org/duo-workflow/default-docker-image/workflow-generic-image:v0.0.4 \
     /tmp/duo-workflow-executor \
     --token="$GDK_TOKEN" --base-url "http://gdk.test:3000" \
     --server "host.rancher-desktop.internal:50052" --goal "$WORKFLOW_GOAL" \
     --workflow-id=$WORKFLOW_ID \
     --realm=$GITLAB_REALM --user-id="$GLOABL_USER_ID" --namespace-id="$GITLAB_PROJECT_NAMESPACE_ID"
   ```

### Query the Workflow checkpoints

To verify that the executor is working you can view the checkpoints in the GitLab instance either via REST or GraphQL API.

#### Via REST

```bash
curl --verbose \
  --header "Authorization: Bearer $YOUR_GITLAB_PAT" \
  $GDK_GITLAB_URL/api/v4/ai/duo_workflows/workflows/$WORKFLOW_ID/checkpoints
```

#### GraphQL

Use this Query via a GraphQL interface:

```graphql
query getDuoWorkflowEvents($workflowId: AiDuoWorkflowsWorkflowID!) {
  duoWorkflowEvents(workflowId: $workflowId) {
    edges {
      node {
        errors,
        metadata,
        workflowStatus,
        workflowGoal
      }
    }
  }
}
```

To set the variables:

```json
{
  "workflowId": "gid://gitlab/Ai::DuoWorkflows::Workflow/1"
}
```

## How to update the protobufs dependency

The protobuf files are centrally located at
https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-service. That project
builds a Go module which is consumed by this project. You can update that
Go module with:

```bash
go get gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb@main
```

## Changing protobufs in both projects locally

If you are editing the `.proto` files locally in Duo Workflow Service and you want to test your changes in Duo Workflow Executor at the same time you can use the [replace directive](https://go.dev/ref/mod#go-mod-file-replace) in `go.mod`:

```golang
replace gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb => ../duo-workflow-service/clients/gopb
```

After making changes to the `.proto` files in `duo-workflow-service` you would need to recompile the `.go` files in that project for the changes to take effect in your Go code.

## Changing protobufs to use a branch in `duo-workflow-service`

You can also point the go module at a branch in `duo-workflow-service` as well
while you are testing things locally and in CI with:

```bash
go get gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb@my-branch-name
```

Be sure to update the branch reference to `main` again when you are finished
testing and when the `duo-workflow-service` changes are merged.

## Contributing

Contributions are welcome.
For development guidelines including code formatting requirements, please see our [contributing guide](CONTRIBUTING.md).
Please see the [maintainership documentation](docs/maintainership.md) if you'd like to become a maintainer.

## Releasing

To release a new version of the Duo Workflow Executor, create a new [tag](https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-executor/-/tags/new). GitLab CI will automatically create a release from the tag.

To use the new release, update the [release version in the main GitLab codebase](https://gitlab.com/gitlab-org/gitlab/-/blob/master/DUO_WORKFLOW_EXECUTOR_VERSION).

## Other links

- [LICENSE](LICENSE)
- [Code of Conduct](CODE_OF_CONDUCT.md)
- [Security Policy](SECURITY.md)

## Logs

When Duo Workflow is run from IDE on local environment, follow this steps to view logs output:

1. `docker ps -a` - find most recent executor container
1. `docker logs [container_id]` - get logs from the container

## Error tracking

We use Sentry for error tracking. For details, refer [error tracking for Duo Workflow Service](docs/error_tracking.md)
