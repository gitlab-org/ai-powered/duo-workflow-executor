package runner

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"

	"go.uber.org/zap"

	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/client"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/git"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/retry"
	service "gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb/contract"
)

type RunnerConfig struct {
	Client         client.Interface
	Version        string
	Goal           string
	Definition     string
	BaseURL        string
	Token          string
	BasePath       string
	WorkflowID     string
	StubsDir       string
	GitConfig      *git.Config
	RecordStubs    bool
	Logger         *zap.Logger
	AnalyticSender actions.AnalyticSender
}

type actionConfig struct {
	action      *service.Action
	basePath    string
	token       string
	gitConfig   *git.Config
	baseURL     string
	stubsDir    string
	recordStubs bool
	logger      *zap.Logger
}

type executionRunner struct {
	client         client.Interface
	version        string
	goal           string
	definition     string
	baseURL        string
	token          string
	basePath       string
	workflowID     string
	stubsDir       string
	gitConfig      *git.Config
	recordStubs    bool
	logger         *zap.Logger
	analyticSender actions.AnalyticSender
}

func NewExecutionRunner(cfg *RunnerConfig) *executionRunner {
	return &executionRunner{
		client:         cfg.Client,
		version:        cfg.Version,
		goal:           cfg.Goal,
		definition:     cfg.Definition,
		baseURL:        cfg.BaseURL,
		token:          cfg.Token,
		basePath:       cfg.BasePath,
		workflowID:     cfg.WorkflowID,
		stubsDir:       cfg.StubsDir,
		gitConfig:      cfg.GitConfig,
		recordStubs:    cfg.RecordStubs,
		logger:         cfg.Logger,
		analyticSender: cfg.AnalyticSender,
	}
}

func (e *executionRunner) Run(ctx context.Context, retries int) error {
	// Start analytics event in the background
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		if retries > 0 {
			e.sendAnalyticsEvent(actions.AnalyticsEventRetry)
		} else {
			e.sendAnalyticsEvent(actions.AnalyticsEventStart)
		}
	}()
	defer wg.Wait()

	// Create a cancellable context
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Start workflow execution
	wf, err := e.client.ExecuteWorkflow(ctx, e.version, e.definition, e.goal, e.workflowID)
	if err != nil {
		return &retry.RetryableError{Msg: fmt.Sprintf("failed to connect: %v", err)}
	}

	// Process workflow actions
	if err := e.processWorkflowActions(ctx, wf); err != nil {
		return err
	}

	if err := wf.CloseSend(); err != nil {
		return fmt.Errorf("error sending close send: %w", err)
	}

	e.sendAnalyticsEvent(actions.AnalyticsEventFinish)
	return nil
}

// processWorkflowActions handles the main action processing loop
func (e *executionRunner) processWorkflowActions(ctx context.Context, wf client.WorkflowStream) error {
	for {
		action, err := wf.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			return &retry.RetryableError{Msg: fmt.Sprintf("error receiving event: %v", err)}
		}

		e.logger.Debug("Received action", zap.Any("action", action))

		result, err := e.executeAction(ctx, action)
		if err != nil {
			return err
		}

		if err := wf.Send(&service.ActionResponse{
			RequestID: action.RequestID,
			Response:  result,
		}); err != nil {
			return &retry.RetryableError{Msg: fmt.Sprintf("error sending response: %v", err)}
		}
	}

	return nil
}

// executeAction prepares and executes a single action
func (e *executionRunner) executeAction(ctx context.Context, action *service.Action) (string, error) {
	actionsCfg := &actionConfig{
		action:      action,
		basePath:    e.basePath,
		token:       e.token,
		gitConfig:   e.gitConfig,
		baseURL:     e.baseURL,
		stubsDir:    e.stubsDir,
		recordStubs: e.recordStubs,
		logger:      e.logger,
	}

	act, err := getActionToExecute(actionsCfg)
	if err != nil {
		return "", fmt.Errorf("failed to get action to execute %w", err)
	}

	result, errExecute := act.Execute(ctx)

	if errExecute != nil {
		e.logger.Error("Failed to run tool", zap.String("result", result))
		errorMessage := "Error running tool: " + errExecute.Error()

		if strings.TrimSpace(result) != "" {
			errorMessage = fmt.Sprintf("%s. Result: %s", errorMessage, result)
		}
		result = errorMessage
	}

	processResult(&result)
	e.logger.Debug("Action result", zap.String("result", result))

	return result, nil
}

func (e *executionRunner) sendAnalyticsEvent(event actions.AnalyticsEventType) {
	err := e.analyticSender.Send(event)
	if err != nil {
		e.logger.Error("Failed to send analytics event", zap.String("workflow_id", e.workflowID), zap.String("event", string(event)), zap.Error(err))
	} else {
		e.logger.Debug("Sent analytics event", zap.String("workflow_id", e.workflowID), zap.String("event", string(event)))
	}
}

// actionCreator is a function type that creates an actions.Action
type actionCreator func(*actionConfig, *os.Root) actions.Action

func getActionToExecute(cfg *actionConfig) (actions.Action, error) {
	if cfg.token == "" {
		return nil, fmt.Errorf("no token provided")
	}

	root, err := os.OpenRoot(cfg.basePath)
	if err != nil {
		return nil, fmt.Errorf("failed to open base path: %w", err)
	}

	creator := getActionCreator(cfg.action)
	if creator != nil {
		return creator(cfg, root), nil
	}

	return nil, fmt.Errorf("unsupported action type: %T", cfg.action.Action)
}

func getActionCreator(action *service.Action) actionCreator {
	switch action.Action.(type) {
	case *service.Action_RunCommand:
		return createRunCommand
	case *service.Action_RunGitCommand:
		return createGitCommand
	case *service.Action_RunReadFile:
		return createReadFile
	case *service.Action_RunWriteFile:
		return createWriteFile
	case *service.Action_RunHTTPRequest:
		return createHTTPRequest
	case *service.Action_RunEditFile:
		return createEditFile
	default:
		return nil
	}
}

func createRunCommand(cfg *actionConfig, root *os.Root) actions.Action {
	commandAction := cfg.action.GetRunCommand()
	return actions.NewRunCommand(
		commandAction.Program,
		cfg.basePath,
		commandAction.Flags,
		commandAction.Arguments,
	)
}

func createGitCommand(cfg *actionConfig, root *os.Root) actions.Action {
	gitConfig := &actions.GitCommandConfig{
		GitConfig: cfg.gitConfig,
		Basepath:  cfg.basePath,
		Logger:    cfg.logger,
	}
	gitCommandAction := cfg.action.GetRunGitCommand()
	return actions.NewRunGitCommand(
		gitCommandAction.Command,
		gitCommandAction.GetArguments(),
		gitCommandAction.RepositoryUrl,
		gitConfig,
	)
}

func createReadFile(cfg *actionConfig, root *os.Root) actions.Action {
	readFileAction := cfg.action.GetRunReadFile()
	return actions.NewReadFile(readFileAction.Filepath, root)
}

func createWriteFile(cfg *actionConfig, root *os.Root) actions.Action {
	writeFileAction := cfg.action.GetRunWriteFile()
	return actions.NewWriteFile(
		writeFileAction.Filepath,
		writeFileAction.Contents,
		root,
	)
}

func createHTTPRequest(cfg *actionConfig, root *os.Root) actions.Action {
	httpRequestAction := cfg.action.GetRunHTTPRequest()
	api := actions.NewGitLabAPI(
		cfg.baseURL,
		cfg.token,
		httpRequestAction.Method,
		httpRequestAction.Path,
		httpRequestAction.Body,
	)
	if len(cfg.stubsDir) > 0 {
		return actions.NewGitLabAPIStubbed(api, cfg.stubsDir, cfg.recordStubs)
	}
	return api
}

func createEditFile(cfg *actionConfig, root *os.Root) actions.Action {
	editFileAction := cfg.action.GetRunEditFile()
	return actions.NewEditFile(
		editFileAction.Filepath,
		editFileAction.OldString,
		editFileAction.NewString,
		root,
	)
}

func processResult(result *string) {
	if len(*result) > client.MaxMessageSize {
		*result = fmt.Sprintf("Error running tool: Maximum allowed size exceeded.: Result: %d bytes. Maximum allowed: %d bytes", len(*result), client.MaxMessageSize)
	}
}
