package runner

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/client"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/git"
	service "gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb/contract"
)

var (
	_ client.Interface      = (*mockClient)(nil)
	_ client.WorkflowStream = (*mockStream)(nil)
)

// mockClient is a mock of client.Interface.
type mockClient struct {
	mock.Mock
}

func (m *mockClient) ExecuteWorkflow(ctx context.Context, version, definition, goal, workflowID string) (client.WorkflowStream, error) {
	args := m.Called(ctx, version, definition, goal, workflowID)
	return args.Get(0).(client.WorkflowStream), args.Error(1)
}

func (m *mockClient) Close() error {
	return m.Called().Error(0)
}

type mockAnalyticSender struct {
	events []string
}

func (m *mockAnalyticSender) Send(event actions.AnalyticsEventType) error {
	m.events = append(m.events, string(event))
	return nil
}

type mockStream struct {
	mock.Mock
	recv chan *service.Action
	send chan *service.ActionResponse
}

func (m *mockStream) Recv() (*service.Action, error) {
	a, ok := <-m.recv
	if !ok {
		return nil, io.EOF
	}
	return a, nil
}

func (m *mockStream) Send(r *service.ActionResponse) error {
	m.send <- r
	return nil
}

func (m *mockStream) CloseSend() error {
	args := m.Called()
	return args.Error(0)
}

func createMockClient() (*mockClient, chan *service.Action, chan *service.ActionResponse) {
	mc := new(mockClient)

	ms := &mockStream{
		recv: make(chan *service.Action, 1),
		send: make(chan *service.ActionResponse, 1),
	}
	ms.On("CloseSend").Return(nil)

	mc.On("ExecuteWorkflow", mock.Anything, "1.0.0", "test-definition", "test-goal", "workflow-123").Return(ms, nil)
	mc.On("Close").Return(nil)

	return mc, ms.recv, ms.send
}

type testRunner struct {
	t              *testing.T
	baseDir        string
	root           *os.Root
	mc             *mockClient
	recv           chan *service.Action
	send           chan *service.ActionResponse
	logger         *zap.Logger
	analyticSender *mockAnalyticSender
}

func setupTest(t *testing.T) *testRunner {
	t.Helper()
	baseDir := t.TempDir()
	root, err := os.OpenRoot(baseDir)
	require.NoError(t, err)
	t.Cleanup(func() { root.Close() })

	mc, recv, send := createMockClient()
	logger := zaptest.NewLogger(t)
	analyticSender := &mockAnalyticSender{}

	return &testRunner{
		t:              t,
		baseDir:        baseDir,
		root:           root,
		mc:             mc,
		recv:           recv,
		send:           send,
		logger:         logger,
		analyticSender: analyticSender,
	}
}

func (tr *testRunner) createRunner() *executionRunner {
	return NewExecutionRunner(&RunnerConfig{
		Client:         tr.mc,
		Version:        "1.0.0",
		Goal:           "test-goal",
		Definition:     "test-definition",
		BaseURL:        "https://gitlab.com",
		Token:          "test-token",
		BasePath:       tr.baseDir,
		WorkflowID:     "workflow-123",
		Logger:         tr.logger,
		AnalyticSender: tr.analyticSender,
		GitConfig: &git.Config{
			HTTPSUser:   "auth",
			HTTPBaseURL: "gitlab.com",
			Password:    "pass",
			UserEmail:   "test@example.com",
		},
	})
}

func (tr *testRunner) createFile(t *testing.T, filename string, content string) error {
	t.Helper()
	file, err := tr.root.Create(filename)
	require.NoError(t, err)
	defer file.Close()

	_, err = file.Write([]byte(content))
	require.NoError(t, err)
	return nil
}

func (tr *testRunner) readFile(t *testing.T, filename string) (string, error) {
	t.Helper()
	file, err := tr.root.Open(filename)
	require.NoError(t, err)
	defer file.Close()

	content, err := io.ReadAll(file)
	require.NoError(t, err)
	return string(content), nil
}

func TestRun(t *testing.T) {
	ctx := context.Background()

	t.Run("when run command action is called it should return the response", func(t *testing.T) {
		tr := setupTest(t)

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunCommand{
				RunCommand: &service.RunCommandAction{
					Program:   "echo",
					Arguments: []string{"Hello!"},
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Equal(t, "Hello!\n", response.Response)
	})

	t.Run("when read file action is called will read a file", func(t *testing.T) {
		tr := setupTest(t)

		testfile := "testfile.txt"
		content := "Hello, World!"
		require.NoError(t, tr.createFile(t, testfile, content))

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunReadFile{
				RunReadFile: &service.ReadFile{
					Filepath: testfile,
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Equal(t, content, response.Response)
	})

	t.Run("when write file action is called will write a file", func(t *testing.T) {
		tr := setupTest(t)

		testfile := "testfile.txt"
		content := "Hello World"

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunWriteFile{
				RunWriteFile: &service.WriteFile{
					Filepath: testfile,
					Contents: content,
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Equal(t, "File written successfully", response.Response)
	})

	t.Run("when edit file action is called will edit a file", func(t *testing.T) {
		tr := setupTest(t)

		testfile := "testfile.txt"
		require.NoError(t, tr.createFile(t, testfile, "Hello World"))

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunEditFile{
				RunEditFile: &service.EditFile{
					Filepath:  testfile,
					OldString: "Hello",
					NewString: "Goodbye",
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		contents, err := tr.readFile(t, testfile)
		require.NoError(t, err)
		require.Equal(t, "Goodbye World", contents)

		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Equal(t, fmt.Sprintf("File '%s' has been updated", testfile), response.Response)
	})

	t.Run("when gitlab API action is called will write a file", func(t *testing.T) {
		tr := setupTest(t)

		responseBody := "test"
		mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			_, err := w.Write([]byte(responseBody))
			require.NoError(t, err)
		}))
		defer mockServer.Close()

		content := "Hello World"

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunHTTPRequest{
				RunHTTPRequest: &service.RunHTTPRequest{
					Method: http.MethodGet,
					Path:   "/test",
					Body:   &content,
				},
			},
		}
		close(tr.recv)

		// Create custom runner with mock server URL
		r := NewExecutionRunner(&RunnerConfig{
			Client:     tr.mc,
			Version:    "1.0.0",
			Goal:       "test-goal",
			Definition: "test-definition",
			BaseURL:    mockServer.URL,
			Token:      "test-token",
			BasePath:   tr.baseDir,
			WorkflowID: "workflow-123",
			GitConfig: &git.Config{
				HTTPSUser:   "auth",
				HTTPBaseURL: "gitlab.com",
				Password:    "pass",
				UserEmail:   "test@example.com",
			},
			Logger:         tr.logger,
			AnalyticSender: tr.analyticSender,
		})

		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Equal(t, responseBody, response.Response)
	})

	t.Run("when action result is too large", func(t *testing.T) {
		tr := setupTest(t)

		testfile := "testfile.txt"
		largeContent := strings.Repeat("a", 5*1024*1024) // 5MB content
		require.NoError(t, tr.createFile(t, testfile, largeContent))

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunReadFile{
				RunReadFile: &service.ReadFile{
					Filepath: testfile,
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		contents, err := tr.readFile(t, testfile)
		require.NoError(t, err)
		require.Equal(t, largeContent, contents)

		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Equal(t, "Error running tool: Maximum allowed size exceeded.: Result: 5242880 bytes. Maximum allowed: 4194304 bytes", response.Response)
	})

	t.Run("when action fails sends an error in the body", func(t *testing.T) {
		tr := setupTest(t)

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunReadFile{
				RunReadFile: &service.ReadFile{
					Filepath: "doesnotexist.txt",
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.Contains(t, response.Response, "doesnotexist.txt: no such file or directory")
	})

	t.Run("when getActionToExecute is called with GetRunGitCommand action", func(t *testing.T) {
		tr := setupTest(t)

		args := "-s"
		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunGitCommand{
				RunGitCommand: &service.RunGitCommand{
					Command:       "status",
					Arguments:     &args,
					RepositoryUrl: "https://gitlab.com/test/repo.git",
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.NotEmpty(t, response.Response)
		assert.NotContains(t, response.Response, "error")
	})

	t.Run("runner should send analytic events", func(t *testing.T) {
		tr := setupTest(t)

		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunCommand{
				RunCommand: &service.RunCommandAction{
					Program:   "echo",
					Arguments: []string{"Hello!"},
				},
			},
		}
		close(tr.recv)

		r := tr.createRunner()
		err := r.Run(ctx, 1)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)
		assert.ElementsMatch(t, []string{"retry_duo_workflow_execution", "finish_duo_workflow_execution"}, tr.analyticSender.events)
	})

	t.Run("when tool execution fails with partial output", func(t *testing.T) {
		tr := setupTest(t)
		t.Cleanup(func() { tr.root.Close() })

		// Simulate a command that produces output but fails
		tr.recv <- &service.Action{
			RequestID: "test-request-id",
			Action: &service.Action_RunCommand{
				RunCommand: &service.RunCommandAction{
					// Use a non-existent command that should fail but might produce stderr output
					Program:   "grep",
					Arguments: []string{"--invalid-flag"},
				},
			},
		}
		close(tr.recv)

		logger := zaptest.NewLogger(t)
		runnerConfig := &RunnerConfig{
			Client:         tr.mc,
			Version:        "1.0.0",
			Goal:           "test-goal",
			Definition:     "test-definition",
			BaseURL:        "https://gitlab.com",
			Token:          "test-token",
			WorkflowID:     "workflow-123",
			Logger:         logger,
			AnalyticSender: &mockAnalyticSender{},
			BasePath:       tr.baseDir,
			GitConfig: &git.Config{
				HTTPSUser:   "auth",
				HTTPBaseURL: "gitlab.com",
				Password:    "pass",
				UserEmail:   "test@example.com",
			},
		}

		r := NewExecutionRunner(runnerConfig)
		err := r.Run(ctx, 0)
		assert.NoError(t, err)

		response := <-tr.send
		assert.Equal(t, "test-request-id", response.RequestID)

		// The response should contain both the error message and any output
		assert.Contains(t, response.Response, "Result: grep: unrecognized option")
		// Should contain the command name in the error
		assert.Contains(t, response.Response, "Error running tool: exit status 2")
	})
}
