package tokenrevoker

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRevoker_RevokeToken(t *testing.T) {
	t.Run("when api returns error it should return error", func(t *testing.T) {
		mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			require.Equal(t, "/oauth/revoke", r.URL.Path)
			require.Equal(t, "POST", r.Method)
			body, err := io.ReadAll(r.Body)
			require.NoError(t, err)
			require.Equal(t, `{"token":"my_token"}`, string(body))
			w.WriteHeader(http.StatusInternalServerError)
		}))
		defer mockServer.Close()

		ctx := context.Background()
		r, _ := NewTokenRevoker(mockServer.URL, "my_token")

		err := r.RevokeToken(ctx)
		require.EqualError(t, err, "could not revoke token: unexpected status code: 500")
	})
	t.Run("when api revokes token successfully it should not return error", func(t *testing.T) {
		mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			require.Equal(t, "/oauth/revoke", r.URL.Path)
			require.Equal(t, "POST", r.Method)
			body, err := io.ReadAll(r.Body)
			require.NoError(t, err)
			require.Equal(t, `{"token":"my_token"}`, string(body))
			w.WriteHeader(http.StatusOK)
		}))
		defer mockServer.Close()

		ctx := context.Background()
		r, _ := NewTokenRevoker(mockServer.URL, "my_token")

		err := r.RevokeToken(ctx)
		require.NoError(t, err)
	})
}
