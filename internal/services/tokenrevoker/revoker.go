package tokenrevoker

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

type APIExecutor interface {
	Execute(ctx context.Context) (string, error)
}

type Revoker struct {
	apiExecutor APIExecutor
}

func NewTokenRevoker(baseURL string, token string) (*Revoker, error) {
	body, err := prepareTokenBody(token)
	if err != nil {
		return nil, fmt.Errorf("could not prepare request body with token: %w", err)
	}

	api := actions.NewGitLabAPI(baseURL, token, http.MethodPost, "/oauth/revoke", &body)

	return &Revoker{
		apiExecutor: api,
	}, nil
}

func (r *Revoker) RevokeToken(ctx context.Context) error {
	_, err := r.apiExecutor.Execute(ctx)
	if err != nil {
		return fmt.Errorf("could not revoke token: %w", err)
	}

	return nil
}

func prepareTokenBody(token string) (string, error) {
	body := struct {
		Token string `json:"token"`
	}{Token: token}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return "", fmt.Errorf("could not marshal body: %w", err)
	}
	return string(jsonBody), nil
}
