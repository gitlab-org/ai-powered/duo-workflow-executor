package workflow_test

import (
	"fmt"
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/workflow"
	"go.uber.org/zap/zaptest"
)

func setupRepo(t *testing.T) string {
	t.Helper()
	dir := t.TempDir()
	// go's git doesn't seem to support cloning bundles
	cmd := exec.Command("git", "clone", "-b", "master", "../git/testdata/test_repo.bundle", dir)
	assert.Nil(t, cmd.Run())
	return dir
}

func TestNewMetadata_WithGitInfo(t *testing.T) {
	wd, _ := os.Getwd()
	url := fmt.Sprintf("%s/../git/testdata/test_repo.bundle", wd)

	tests := []struct {
		input    string
		expected string
	}{
		{"invalid", "invalid"},
		{"{}", "{}"},
		{"{\"extended_logging\":false}", "{\"extended_logging\":false}"},
		{"{\"extended_logging\":value}", "{\"extended_logging\":value}"},
		{"{\"extended_logging\":1}", "{\"extended_logging\":1}"},
		{"{\"extended_logging\":true}", fmt.Sprintf("{\"extended_logging\":true,\"git_sha\":\"b9f96f158c14892609e800875d2bf689c525218a\",\"git_url\":\"%s\"}", url)},
	}

	dir := setupRepo(t)
	for _, tt := range tests {
		result := workflow.NewMetadata(tt.input, zaptest.NewLogger(t)).WithGitInfo(dir)
		assert.Equal(t, tt.expected, result)
	}
}
