package workflow

import (
	"encoding/json"

	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/git"
	"go.uber.org/zap"
)

type Metadata struct {
	origStr string
	logger  *zap.Logger
}

func NewMetadata(jsonStr string, logger *zap.Logger) *Metadata {
	if len(jsonStr) == 0 {
		jsonStr = "{}"
	}

	return &Metadata{
		origStr: jsonStr,
		logger:  logger,
	}
}

func (m *Metadata) WithGitInfo(basePath string) string {
	// an empty interface is used intentionally to avoid hardcoding expected
	// metadata format in executor so we don't have to update it here whenever
	// it's changed on Rails side
	var meta map[string]interface{}

	err := json.Unmarshal([]byte(m.origStr), &meta)
	if err != nil {
		m.logger.Error("failed to parse metadata", zap.Error(err))
		return m.origStr
	}

	logging, ok := meta["extended_logging"].(bool)
	if !ok || !logging {
		return m.origStr
	}

	repo := git.NewRepository(basePath, m.logger)
	if repo == nil {
		m.logger.Error("failed to get git info", zap.Error(err))
		return m.origStr
	}

	meta["git_url"] = repo.DefaultURL()
	meta["git_sha"] = repo.LastSHA()

	b, err := json.Marshal(meta)
	if err != nil {
		m.logger.Error("failed to create JSON", zap.Error(err))
		return m.origStr
	}
	return string(b)
}
