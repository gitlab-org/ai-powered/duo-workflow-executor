package client_test

import (
	"context"
	"io"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/client"
	service "gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb/contract"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func startServer(t *testing.T, mockServer *mockDuoWorkflowServer) func() {
	t.Helper()

	lis, err := net.Listen("tcp", ":8001")
	require.NoError(t, err)

	s := grpc.NewServer()
	service.RegisterDuoWorkflowServer(s, mockServer)
	go func() {
		err := s.Serve(lis)
		require.NoError(t, err)
	}()

	return func() {
		s.GracefulStop()
	}
}

func closeT(t *testing.T, c io.Closer) {
	t.Helper()
	assert.NoError(t, c.Close())
}

type mockDuoWorkflowServer struct {
	sendChannel                    chan *service.Action
	receiveChannel                 chan string
	waitForActionResponse          bool
	assertMetadataFn               func(md metadata.MD)
	startRequestClientCapabilities []string
	service.UnimplementedDuoWorkflowServer
}

func (m *mockDuoWorkflowServer) ExecuteWorkflow(server service.DuoWorkflow_ExecuteWorkflowServer) error {
	if m.sendChannel != nil {
		for msg := range m.sendChannel {
			err := server.Send(msg)
			if err != nil {
				return err
			}
		}
	}

	for {
		req, err := server.Recv()
		if err != nil {
			return err
		}
		startReq := req.GetStartRequest()
		if startReq != nil {
			m.startRequestClientCapabilities = startReq.ClientCapabilities
			m.receiveChannel <- startReq.ClientVersion
			if !m.waitForActionResponse {
				close(m.receiveChannel)
				break
			}
		}

		md, ok := metadata.FromIncomingContext(server.Context())
		if !ok {
			return err
		}
		m.assertMetadataFn(md)

		actionRes := req.GetActionResponse()
		if actionRes != nil {
			m.receiveChannel <- actionRes.RequestID
			close(m.receiveChannel)
		}
	}

	return nil
}

func TestNewGRPCClient(t *testing.T) {
	logger := zaptest.NewLogger(t)
	defer func() {
		err := logger.Sync()
		require.NoError(t, err)
	}()

	clientMetadata := &client.ClientMetadata{}
	c, err := client.New(logger, "", clientMetadata, false, nil)
	require.NoError(t, err)
	defer closeT(t, c)
	assert.NotNil(t, c)
}

func setupLogger(t *testing.T) (*zap.Logger, func()) {
	t.Helper()
	logger := zaptest.NewLogger(t)
	return logger, func() {
		err := logger.Sync()
		require.NoError(t, err)
	}
}

func TestGRPCClient_ExecuteWorkflow(t *testing.T) {
	t.Run("when no server is not started throws error", func(t *testing.T) {
		logger, teardownLogger := setupLogger(t)
		defer teardownLogger()

		clientMetadata := &client.ClientMetadata{}
		grpcClient, err := client.New(logger, "localhost:8001", clientMetadata, true, nil)
		require.NoError(t, err)
		defer closeT(t, grpcClient)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		_, err = grpcClient.ExecuteWorkflow(ctx, "1.0.0", "", "", "workflow-123")
		assert.ErrorContains(t, err, `rpc error: code = Unavailable desc = connection error: desc = "transport: Error while dialing: dial tcp`)
	})

	t.Run("when server is started successfully connects", func(t *testing.T) {
		logger, teardownLogger := setupLogger(t)
		defer teardownLogger()

		receiveChan := make(chan string)
		mockServer := &mockDuoWorkflowServer{
			receiveChannel:   receiveChan,
			assertMetadataFn: func(md metadata.MD) {},
		}
		stopServer := startServer(t, mockServer)
		defer stopServer()

		clientMetadata := &client.ClientMetadata{}
		grpcClient, err := client.New(logger, "localhost:8001", clientMetadata, true, nil)
		require.NoError(t, err)
		defer closeT(t, grpcClient)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		_, err = grpcClient.ExecuteWorkflow(ctx, "1.0.0", "", "", "workflow-123")
		require.NoError(t, err)

		got := <-receiveChan
		assert.Equal(t, "1.0.0", got)
	})

	t.Run("when server sends events those are available via Recv", func(t *testing.T) {
		logger, teardownLogger := setupLogger(t)
		defer teardownLogger()

		receiveChan := make(chan string)
		sendChan := make(chan *service.Action)
		mockServer := &mockDuoWorkflowServer{
			sendChannel:      sendChan,
			receiveChannel:   receiveChan,
			assertMetadataFn: func(md metadata.MD) {},
		}
		stopServer := startServer(t, mockServer)
		defer stopServer()

		clientMetadata := &client.ClientMetadata{}
		grpcClient, err := client.New(logger, "localhost:8001", clientMetadata, true, nil)
		require.NoError(t, err)
		defer closeT(t, grpcClient)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		wf, err := grpcClient.ExecuteWorkflow(ctx, "1.0.0", "", "", "workflow-123")
		require.NoError(t, err)

		go func() {
			defer close(sendChan)
			sendChan <- &service.Action{
				Action: &service.Action_RunCommand{
					RunCommand: &service.RunCommandAction{
						Program:   "echo",
						Arguments: []string{"123"},
					},
				},
			}
		}()

		msg, err := wf.Recv()
		require.NoError(t, err)
		require.NotNil(t, msg)
		assert.Equal(t, "echo", msg.GetRunCommand().GetProgram())
		assert.Equal(t, []string{"123"}, msg.GetRunCommand().GetArguments())

		got := <-receiveChan
		assert.Equal(t, "1.0.0", got)
	})
	t.Run("when metadata is provided", func(t *testing.T) {
		logger, teardownLogger := setupLogger(t)
		defer teardownLogger()

		receiveChan := make(chan string)
		sendChan := make(chan *service.Action)
		mockServer := &mockDuoWorkflowServer{
			receiveChannel:        receiveChan,
			sendChannel:           sendChan,
			waitForActionResponse: true,
			assertMetadataFn: func(md metadata.MD) {
				require.Equal(t, "bearer dws-token", md.Get("authorization")[0])
				require.Equal(t, "oidc", md.Get("x-gitlab-authentication-type")[0])
				require.Equal(t, "saas", md.Get("x-gitlab-realm")[0])
				require.Equal(t, "globalUserID", md.Get("x-gitlab-global-user-id")[0])
				require.Equal(t, "gitlab-token", md.Get("x-gitlab-oauth-token")[0])
				require.Equal(t, "gdk.test:3000", md.Get("x-gitlab-base-url")[0])
				require.Equal(t, "instance-id", md.Get("x-gitlab-instance-id")[0])
				require.Equal(t, "namespace-id", md.Get("x-gitlab-namespace-id")[0])
			},
		}
		stopServer := startServer(t, mockServer)
		defer stopServer()

		grpcClient, err := client.New(logger, "localhost:8001", &client.ClientMetadata{
			DuoWorkflowServiceToken: "dws-token",
			DuoWorkflowServiceRealm: "saas",
			DuoWorkflowGlobalUserID: "globalUserID",
			GitlabToken:             "gitlab-token",
			GitlabBaseURL:           "gdk.test:3000",
			InstanceID:              "instance-id",
			NamespaceID:             "namespace-id",
		}, true, nil)
		require.NoError(t, err)
		defer closeT(t, grpcClient)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		wf, err := grpcClient.ExecuteWorkflow(ctx, "1.0.0", "", "", "workflow-123")
		require.NoError(t, err)

		go func() {
			defer close(sendChan)
			sendChan <- &service.Action{
				Action: &service.Action_RunCommand{
					RunCommand: &service.RunCommandAction{
						Program:   "echo",
						Arguments: []string{"123"},
					},
				},
			}
		}()

		_, err = wf.Recv()
		require.NoError(t, err)
		<-receiveChan
	})

	t.Run("when capabilities are provided", func(t *testing.T) {
		logger, teardownLogger := setupLogger(t)
		defer teardownLogger()

		receiveChan := make(chan string)
		mockServer := &mockDuoWorkflowServer{
			receiveChannel:        receiveChan,
			waitForActionResponse: true,
			assertMetadataFn:      func(md metadata.MD) {},
		}
		stopServer := startServer(t, mockServer)
		defer stopServer()

		grpcClient, err := client.New(logger, "localhost:8001", &client.ClientMetadata{}, true, []string{"enable_user_input_tool"})
		require.NoError(t, err)
		defer closeT(t, grpcClient)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		_, err = grpcClient.ExecuteWorkflow(ctx, "1.0.0", "", "", "workflow-123")
		require.NoError(t, err)
		<-receiveChan
		require.Equal(t, 1, len(mockServer.startRequestClientCapabilities))
		require.Equal(t, "enable_user_input_tool", mockServer.startRequestClientCapabilities[0])
	})
}

func TestGRPCClient_SendResponses(t *testing.T) {
	logger, teardownLogger := setupLogger(t)
	defer teardownLogger()

	sendChan := make(chan *service.Action)
	receiveChan := make(chan string)
	mockServer := &mockDuoWorkflowServer{
		sendChannel:           sendChan,
		receiveChannel:        receiveChan,
		waitForActionResponse: true,
		assertMetadataFn:      func(md metadata.MD) {},
	}
	stopServer := startServer(t, mockServer)
	defer stopServer()

	clientMetadata := &client.ClientMetadata{}
	grpcClient, err := client.New(logger, "localhost:8001", clientMetadata, true, nil)
	require.NoError(t, err)
	defer closeT(t, grpcClient)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wf, err := grpcClient.ExecuteWorkflow(ctx, "1.0.0", "", "", "workflow-123")
	require.NoError(t, err)

	go func() {
		defer close(sendChan)

		sendErr := wf.Send(&service.ActionResponse{
			RequestID: "id123",
		})
		assert.NoError(t, sendErr)
	}()

	for _, expectation := range []string{"1.0.0", "id123"} {
		got := <-receiveChan
		assert.Equal(t, expectation, got)
	}
}
