package client

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"time"

	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"

	service "gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb/contract"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"

	"gitlab.com/gitlab-org/labkit/correlation"
)

// MaxMessageSize This value of 4 MB is equal to the default maximum gRPC message size
// We're explicitly setting it and exporting to be able to use in `runner` so that we can
// limit the  action response we're sending to Duo Workflow Service.
const MaxMessageSize = 4 * 1024 * 1024 // 4MB

type Interface interface {
	// ExecuteWorkflow starts listening for workflow actions to execute.
	// Make sure to cancel the passed context on error and when WorkflowStream is no longer needed.
	ExecuteWorkflow(ctx context.Context, version, definition, goal, workflowID string) (WorkflowStream, error)
	Close() error
}

type ClientMetadata struct {
	GitlabToken             string
	GitlabBaseURL           string
	DuoWorkflowServiceToken string
	DuoWorkflowServiceRealm string
	InstanceID              string
	DuoWorkflowGlobalUserID string
	WorkflowMetadata        string
	NamespaceID             string
}

type impl struct {
	logger             *zap.Logger
	conn               *grpc.ClientConn
	client             service.DuoWorkflowClient
	metadata           *ClientMetadata
	clientCapabilities []string
}

func New(logger *zap.Logger, serverAddr string, metadata *ClientMetadata, insecureChannel bool, clientCapabilities []string) (Interface, error) {
	opts := []grpc.DialOption{
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                20 * time.Second, // send pings every 20 seconds if there is no activity
			PermitWithoutStream: true,
		}),
	}

	if insecureChannel {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		tlsConfig := &tls.Config{
			MinVersion: tls.VersionTLS12,
		}
		creds := credentials.NewTLS(tlsConfig)
		opts = append(opts, grpc.WithTransportCredentials(creds))
	}

	serviceConfig := `{
	    "methodConfig": [{
	        "name": [{"service": "DuoWorkflow"}],
	        "retryPolicy": {
	            "maxAttempts": 4,
	            "initialBackoff": "0.1s",
	            "maxBackoff": "1s",
	            "backoffMultiplier": 2,
	            "RetryableStatusCodes": [ "UNAVAILABLE" ]
	        }
	    }]
	}`

	callOptions := grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(MaxMessageSize), grpc.MaxCallSendMsgSize(MaxMessageSize))
	opts = append(opts, grpc.WithDefaultServiceConfig(serviceConfig), callOptions)

	conn, err := grpc.NewClient(serverAddr, opts...)
	if err != nil {
		return nil, err
	}

	c := service.NewDuoWorkflowClient(conn)

	return &impl{
		logger:             logger,
		conn:               conn,
		client:             c,
		metadata:           metadata,
		clientCapabilities: clientCapabilities,
	}, nil
}

func (c *impl) setAuthHeaders(ctx context.Context) context.Context {
	if c.metadata == nil {
		c.logger.Info("no metadata provided, not setting auth headers")
		return ctx
	}

	md := metadata.New(map[string]string{
		"authorization":                fmt.Sprintf("bearer %s", c.metadata.DuoWorkflowServiceToken),
		"x-gitlab-authentication-type": "oidc",
		"x-gitlab-realm":               c.metadata.DuoWorkflowServiceRealm,
		"x-gitlab-global-user-id":      c.metadata.DuoWorkflowGlobalUserID,
		"x-gitlab-oauth-token":         c.metadata.GitlabToken,
		"x-gitlab-base-url":            c.metadata.GitlabBaseURL,
		"x-gitlab-instance-id":         c.metadata.InstanceID,
		"x-request-id":                 correlation.ExtractFromContext(ctx),
		"x-gitlab-namespace-id":        c.metadata.NamespaceID,
	})

	return metadata.NewOutgoingContext(ctx, md)
}

func (c *impl) ExecuteWorkflow(ctx context.Context, version, definition, goal, workflowID string) (WorkflowStream, error) {
	ctx = c.setAuthHeaders(ctx)

	stream, err := c.client.ExecuteWorkflow(ctx)
	if err != nil {
		return nil, err
	}
	// Send capabilities to start the workflow
	// TODO: Send GitLab server version in request
	err = stream.Send(&service.ClientEvent{
		Response: &service.ClientEvent_StartRequest{
			StartRequest: &service.StartWorkflowRequest{
				ClientVersion:      version,
				WorkflowDefinition: definition,
				Goal:               goal,
				WorkflowID:         workflowID,
				WorkflowMetadata:   c.metadata.WorkflowMetadata,
				ClientCapabilities: c.clientCapabilities,
			},
		},
	})
	if err != nil {
		if err == io.EOF {
			// Recv() returns the actual error for client streams.
			_, err = stream.Recv()
		}
		return nil, err
	}
	return &workflowStream{
		stream: stream,
	}, nil
}

func (c *impl) Close() error {
	err := c.conn.Close()
	return err
}
