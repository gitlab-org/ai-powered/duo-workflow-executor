package client

import (
	"io"

	service "gitlab.com/gitlab-org/duo-workflow/duo-workflow-service/clients/gopb/contract"
)

type WorkflowStream interface {
	Recv() (*service.Action, error)
	Send(*service.ActionResponse) error
	CloseSend() error
}

type workflowStream struct {
	stream service.DuoWorkflow_ExecuteWorkflowClient
}

func (s *workflowStream) Recv() (*service.Action, error) {
	return s.stream.Recv()
}

func (s *workflowStream) Send(response *service.ActionResponse) error {
	err := s.stream.Send(&service.ClientEvent{
		Response: &service.ClientEvent_ActionResponse{
			ActionResponse: response,
		},
	})
	if err != nil {
		if err == io.EOF {
			// Recv() returns the actual error for client streams.
			_, err = s.stream.Recv()
			return err
		}
		return err
	}
	return nil
}

func (s *workflowStream) CloseSend() error {
	return s.stream.CloseSend()
}
