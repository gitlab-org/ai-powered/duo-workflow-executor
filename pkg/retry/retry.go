package retry

import (
	"context"
	"errors"
	"math"
	"time"

	"go.uber.org/zap"
)

type Runnable interface {
	Run(context.Context, int) error
}

type RetryableError struct {
	Msg string
}

func (e *RetryableError) Error() string {
	return e.Msg
}

type RetryConfig struct {
	multiplier   float64
	initialDelay float64
	maxDelay     float64
	maxRetries   int
}

func WithRetry(ctx context.Context, runner Runnable, logger *zap.Logger, config *RetryConfig) error {
	if config == nil {
		config = &RetryConfig{
			multiplier:   2.5,
			initialDelay: 10.0,
			maxDelay:     180.0,
			maxRetries:   3,
		}
	}

	var err error

	for retries := 0; retries <= config.maxRetries; retries++ {
		err = runner.Run(ctx, retries)
		if err == nil {
			break
		}

		var retErr *RetryableError
		if !errors.As(err, &retErr) || retries == config.maxRetries {
			break
		}

		delay := math.Min(
			config.initialDelay*math.Pow(config.multiplier, float64(retries)),
			config.maxDelay,
		)
		logger.Warn("Runner failed, will retry after delay",
			zap.Error(err),
			zap.Float64("retry_delay", delay))
		time.Sleep(time.Duration(delay) * time.Second)
	}
	return err
}
