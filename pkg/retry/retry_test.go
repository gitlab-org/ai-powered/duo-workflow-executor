package retry

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
)

type mockRunner struct {
	Counter int
	result  error
}

func (r *mockRunner) Run(_ context.Context, _ int) error {
	r.Counter++
	return r.result
}

func TestWithRetry(t *testing.T) {
	logger := zaptest.NewLogger(t)
	config := &RetryConfig{
		multiplier:   0,
		initialDelay: 0.0,
		maxDelay:     0.0,
		maxRetries:   3,
	}

	ctx := context.Background()

	t.Run("when first run passes", func(t *testing.T) {
		runner := &mockRunner{result: nil}
		result := WithRetry(ctx, runner, logger, config)
		assert.Equal(t, nil, result)
		assert.Equal(t, 1, runner.Counter)
	})

	t.Run("when run returns not retryable error", func(t *testing.T) {
		err := errors.New("some error")
		runner := &mockRunner{result: err}
		result := WithRetry(ctx, runner, logger, config)
		assert.Equal(t, err, result)
		assert.Equal(t, 1, runner.Counter)
	})

	t.Run("when run returns retryable error", func(t *testing.T) {
		err := &RetryableError{Msg: "retryable error"}
		runner := &mockRunner{result: err}
		result := WithRetry(ctx, runner, logger, config)
		assert.Equal(t, err, result)
		assert.Equal(t, 4, runner.Counter)
	})
}
