package actions_test

import (
	"context"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

func TestWriteFile_Execute(t *testing.T) {
	// Create base directory for testing
	baseDir := t.TempDir()
	require.NoError(t, os.MkdirAll(
		filepath.Join(baseDir, "subdir"),
		0o755,
	))

	// Create root for base directory
	root, err := os.OpenRoot(baseDir)
	require.NoError(t, err)
	defer root.Close()

	tests := []struct {
		name     string
		filepath string
		contents string
		want     string
		wantErr  bool
	}{
		{
			name:     "write to file successfully",
			filepath: "testfile.txt",
			contents: "Hello World",
			want:     "File written successfully",
			wantErr:  false,
		},
		{
			name:     "write to file in subdirectory",
			filepath: "subdir/testfile.txt",
			contents: "Hello World",
			want:     "File written successfully",
			wantErr:  false,
		},
		{
			name:     "write to file with invalid path",
			filepath: "../outside.txt",
			contents: "Hello World",
			want:     "",
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wf := actions.NewWriteFile(tt.filepath, tt.contents, root)
			ctx := context.Background()
			result, err := wf.Execute(ctx)

			if tt.wantErr {
				require.Error(t, err)
				if tt.name == "write to file with invalid path" {
					require.Contains(t, err.Error(), "path escapes from parent")
				}
				return
			}

			require.NoError(t, err)
			require.Equal(t, tt.want, result)

			content, err := os.ReadFile(filepath.Join(baseDir, tt.filepath))
			require.NoError(t, err)
			require.Equal(t, tt.contents, string(content))
		})
	}
}
