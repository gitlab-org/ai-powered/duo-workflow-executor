package actions

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/gitlab-org/labkit/correlation"
)

// An HTTP client to perform GitLab API requests
// Clients are safe for concurrent use: https://pkg.go.dev/net/http#Client
var httpClient = &http.Client{ // nolint:gochecknoglobals
	Transport: correlation.NewInstrumentedRoundTripper(http.DefaultTransport),
}

type gitLabAPI struct {
	baseURL string
	token   string
	method  string
	path    string
	body    *string
}

type errorResponse struct {
	Message string `json:"message"`
	Status  *int   `json:"status"`
}

func NewGitLabAPI(baseURL string, token string, method string, path string, body *string) *gitLabAPI {
	return &gitLabAPI{
		baseURL: baseURL,
		token:   token,
		method:  method,
		path:    path,
		body:    body,
	}
}

func (a *gitLabAPI) Execute(ctx context.Context) (string, error) {
	url := a.URL()

	var bodyBuffer bytes.Buffer
	if a.body != nil {
		bodyBuffer.WriteString(*a.body)
	}
	req, err := http.NewRequestWithContext(ctx, a.method, url, &bodyBuffer)
	if err != nil {
		return "", err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", a.token))
	if a.body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	resp, err := httpClient.Do(req)
	if err != nil {
		errResp := errorResponse{
			Message: fmt.Sprintf("http client failed to make a request with error %s", err),
			Status:  nil,
		}
		jsonRsp, _ := json.Marshal(errResp)
		return string(jsonRsp), err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		errMsg := fmt.Sprintf("unexpected status code: %d", resp.StatusCode)
		errResp := errorResponse{
			Message: errMsg,
			Status:  &resp.StatusCode,
		}
		jsonRsp, _ := json.Marshal(errResp)
		return string(jsonRsp), errors.New(errMsg)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		errResp := errorResponse{
			Message: fmt.Sprintf("http client failed to make a request with error %s", err),
			Status:  nil,
		}
		jsonRsp, _ := json.Marshal(errResp)
		return string(jsonRsp), err
	}

	return string(body), nil
}

func (a *gitLabAPI) URL() string {
	return fmt.Sprintf("%s%s", a.baseURL, a.path)
}

func (a *gitLabAPI) MethodPath() string {
	return fmt.Sprintf("%s_%s", a.method, a.path)
}
