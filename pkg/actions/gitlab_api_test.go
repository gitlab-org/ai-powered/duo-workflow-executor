package actions_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

func TestGitLabAPI_Execute(t *testing.T) {
	tests := []struct {
		name       string
		method     string
		path       string
		body       string
		mockStatus int
		mockBody   string
		want       string
		wantErr    bool
	}{
		{
			name:       "successful GET request",
			method:     http.MethodGet,
			path:       "/api/v4/projects/123",
			body:       "",
			mockStatus: http.StatusOK,
			mockBody:   `{"id": 123, "name": "example-project"}`,
			want:       `{"id": 123, "name": "example-project"}`,
			wantErr:    false,
		},
		{
			name:       "successful POST request",
			method:     http.MethodPost,
			path:       "/api/v4/projects",
			body:       `{"name": "new-project"}`,
			mockStatus: http.StatusCreated,
			mockBody:   `{"id": 124, "name": "new-project"}`,
			want:       `{"id": 124, "name": "new-project"}`,
			wantErr:    false,
		},
		{
			name:       "API call with error status",
			method:     http.MethodGet,
			path:       "/api/v4/projects/123",
			body:       "",
			mockStatus: http.StatusNotFound,
			mockBody:   `{"message": "404 Project Not Found"}`,
			want:       "{\"message\":\"unexpected status code: 404\",\"status\":404}",
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(tt.mockStatus)
				_, err := w.Write([]byte(tt.mockBody))
				require.NoError(t, err)
			}))
			defer mockServer.Close()

			api := actions.NewGitLabAPI(mockServer.URL, "example-token", tt.method, tt.path, &tt.body)
			ctx := context.Background()
			got, err := api.Execute(ctx)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestGitLabAPI_ExecuteIntegration(t *testing.T) {
	token := os.Getenv("GITLAB_TOKEN")
	if token == "" {
		t.Skip("GITLAB_TOKEN is not set")
	}

	tests := []struct {
		name     string
		baseURL  string
		method   string
		path     string
		body     string
		wantErr  bool
		contains []string
	}{
		{
			name:     "successful API call",
			baseURL:  "https://gitlab.com",
			method:   http.MethodGet,
			path:     "/api/v4/projects/58711783",
			body:     "",
			wantErr:  false,
			contains: []string{"id", "name"},
		},
		{
			name:    "API call with error",
			baseURL: "https://gitlab.com",
			method:  http.MethodGet,
			path:    "/api/v4/projects/doesnotexist",
			body:    "",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			api := actions.NewGitLabAPI(tt.baseURL, token, tt.method, tt.path, &tt.body)
			ctx := context.Background()
			result, err := api.Execute(ctx)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				for _, substring := range tt.contains {
					assert.Contains(t, result, substring)
				}
			}
		})
	}
}
