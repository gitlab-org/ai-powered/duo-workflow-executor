package actions

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSendAnalyticsEvent_Execute(t *testing.T) {
	tests := []struct {
		name       string
		token      string
		workflowID string
		event      AnalyticsEventType
		mockStatus int
		wantErr    bool
	}{
		{
			name:       "Event sent successfully",
			token:      "example-token",
			workflowID: "1",
			event:      AnalyticsEventStart,
			mockStatus: http.StatusOK,
			wantErr:    false,
		},
		{
			name:       "Unknown event",
			token:      "example-token",
			workflowID: "1",
			event:      "test-event",
			mockStatus: http.StatusUnprocessableEntity,
			wantErr:    true,
		},
		{
			name:       "Token error",
			token:      "invalid-token",
			workflowID: "1",
			event:      AnalyticsEventStart,
			mockStatus: http.StatusUnauthorized,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(tt.mockStatus)
			}))
			defer mockServer.Close()

			a := &SendAnalyticsEvent{
				token:      tt.token,
				baseURL:    mockServer.URL,
				workflowID: tt.workflowID,
			}
			err := a.Send(tt.event)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
