package actions

import (
	"context"
	"os"
	"path/filepath"
	"regexp"

	"go.uber.org/zap"
)

type realAPI interface {
	URL() string
	MethodPath() string
	Execute(context.Context) (string, error)
}

type gitLabAPIStubbed struct {
	api         realAPI
	stubsDir    string
	recordStubs bool
}

func NewGitLabAPIStubbed(api realAPI, stubsDir string, recordStubs bool) *gitLabAPIStubbed {
	return &gitLabAPIStubbed{
		api:         api,
		stubsDir:    stubsDir,
		recordStubs: recordStubs,
	}
}

func (a *gitLabAPIStubbed) Execute(ctx context.Context) (string, error) {
	stubbedResponse := a.StubbedResponse()
	if len(stubbedResponse) > 0 {
		return stubbedResponse, nil
	}

	body, err := a.api.Execute(ctx)
	if err != nil {
		return body, err
	}

	if a.recordStubs {
		a.StoreResponse(body)
	}

	return body, nil
}

func (a *gitLabAPIStubbed) StoreResponse(body string) {
	if len(a.stubsDir) == 0 {
		return
	}

	path := a.stubPath()
	err := os.WriteFile(path, []byte(body), 0o644)
	if err != nil {
		zap.L().Warn("failed to store response", zap.String("path", path), zap.String("error", err.Error()))
	}
	zap.L().Info("stored stub", zap.String("path", path))
}

func (a *gitLabAPIStubbed) stubPath() string {
	url := a.api.MethodPath()
	re := regexp.MustCompile(`[\W]`)
	fname := re.ReplaceAllLiteralString(url, "_")
	return filepath.Join(a.stubsDir, fname)
}

func (a *gitLabAPIStubbed) StubbedResponse() string {
	if len(a.stubsDir) == 0 {
		return ""
	}

	path := a.stubPath()
	content, err := os.ReadFile(path)
	if err != nil {
		zap.L().Warn("stub file not found", zap.String("path", path), zap.String("url", a.api.URL()))
		return ""
	}

	zap.L().Info("using stubbed response", zap.String("path", path))
	return string(content)
}
