package actions

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type AnalyticsEventType string

const (
	AnalyticsEventStart  AnalyticsEventType = "start_duo_workflow_execution"
	AnalyticsEventRetry  AnalyticsEventType = "retry_duo_workflow_execution"
	AnalyticsEventFinish AnalyticsEventType = "finish_duo_workflow_execution"
)

type AdditionalProperties struct {
	WorkflowID string `json:"workflow_id"`
}

type AnalyticsEvent struct {
	Event                AnalyticsEventType    `json:"event"`
	AdditionalProperties *AdditionalProperties `json:"additional_properties"`
	SendEventToSnowplow  bool                  `json:"send_to_snowplow"`
}

type AnalyticSender interface {
	Send(AnalyticsEventType) error
}

type SendAnalyticsEvent struct {
	token      string
	baseURL    string
	workflowID string
}

func NewSendAnalyticsEvent(token, baseURL, workflowID string) *SendAnalyticsEvent {
	return &SendAnalyticsEvent{
		token:      token,
		baseURL:    baseURL,
		workflowID: workflowID,
	}
}

const analyticsPath = "/api/v4/usage_data/track_event"

func (a *SendAnalyticsEvent) Send(event AnalyticsEventType) error {
	url := fmt.Sprintf("%s%s", a.baseURL, analyticsPath)

	payload := AnalyticsEvent{event, &AdditionalProperties{a.workflowID}, true}
	body, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	var bodyBuffer bytes.Buffer
	if body != nil {
		bodyBuffer.WriteString(string(body))
	}
	req, err := http.NewRequest("POST", url, &bodyBuffer)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", a.token))
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = fmt.Errorf("%d: error occurred in sending analytics event", resp.StatusCode)
	}
	return err
}
