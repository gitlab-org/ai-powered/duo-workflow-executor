package actions_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

func TestReadFile_Execute(t *testing.T) {
	// Create base directory for testing
	baseDir := t.TempDir()

	// Create root for base directory
	root, err := os.OpenRoot(baseDir)
	require.NoError(t, err)
	defer root.Close()

	// Create test file within base directory
	testfile := "testfile.txt"
	content := "Hello, World!"
	file, err := root.Create(testfile)
	require.NoError(t, err)
	_, err = file.Write([]byte(content))
	require.NoError(t, err)
	require.NoError(t, file.Close())

	tests := []struct {
		name             string
		fileName         string
		expectedContents string
		wantErr          bool
	}{
		{
			name:             "file exists in base directory",
			fileName:         "testfile.txt",
			expectedContents: content,
			wantErr:          false,
		},
		{
			name:             "file does not exist",
			fileName:         "nonexistentfile.txt",
			expectedContents: "",
			wantErr:          true,
		},
		{
			name:             "attempt to access outside base directory",
			fileName:         "../outside.txt",
			expectedContents: "",
			wantErr:          true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rf := actions.NewReadFile(tt.fileName, root)
			ctx := context.Background()
			result, err := rf.Execute(ctx)

			if tt.wantErr {
				require.Error(t, err)
				if tt.fileName == "../outside.txt" {
					require.Contains(t, err.Error(), "path escapes from parent")
					return
				} else {
					require.Contains(t, err.Error(), "no such file or directory")
					return
				}
			}
			require.NoError(t, err)
			require.Equal(t, tt.expectedContents, result)
		})
	}
}
