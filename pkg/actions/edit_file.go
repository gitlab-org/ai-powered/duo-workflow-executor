package actions

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
)

type editFile struct {
	fileName  string
	oldString string
	newString string
	root      *os.Root
}

func NewEditFile(fileName, oldString, newString string, root *os.Root) *editFile {
	return &editFile{
		fileName:  fileName,
		oldString: oldString,
		newString: newString,
		root:      root,
	}
}

func (a *editFile) Execute(ctx context.Context) (string, error) {
	// Open file for reading
	file, err := a.root.Open(a.fileName)
	if err != nil {
		return "", fmt.Errorf("unable to open file: %w", err)
	}
	defer file.Close()

	// Read the file content
	content, err := io.ReadAll(file)
	if err != nil {
		return "", fmt.Errorf("unable to read file: %w", err)
	}

	// Replace oldString with newString
	updatedContent := bytes.ReplaceAll(content, []byte(a.oldString), []byte(a.newString))

	// Check if any changes were made
	if bytes.Equal(updatedContent, content) {
		return fmt.Sprintf("No changes made to '%s' as '%s' not found.", a.fileName, a.oldString), nil
	}

	// Get file info for permissions
	fileInfo, err := a.root.Stat(a.fileName)
	if err != nil {
		return "", fmt.Errorf("unable to get file info: %w", err)
	}

	// Create file for writing with same permissions
	newFile, err := a.root.OpenFile(a.fileName, os.O_WRONLY|os.O_TRUNC, fileInfo.Mode())
	if err != nil {
		return "", fmt.Errorf("unable to open file for writing: %w", err)
	}
	defer newFile.Close()

	// Write updated content
	_, err = newFile.Write(updatedContent)
	if err != nil {
		return "", fmt.Errorf("unable to write file: %w", err)
	}

	return fmt.Sprintf("File '%s' has been updated", a.fileName), nil
}
