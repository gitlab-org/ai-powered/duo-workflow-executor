package actions

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"go.uber.org/zap"

	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/git"
)

type runGitCommand struct {
	command       string
	arguments     string
	basepath      string
	repositoryURL string
	git           *git.Config
	logger        *zap.Logger
}

type GitCommandConfig struct {
	GitConfig *git.Config
	Basepath  string
	Logger    *zap.Logger
}

func NewRunGitCommand(command string, arguments string, repositoryURL string, config *GitCommandConfig) *runGitCommand {
	return &runGitCommand{
		command:       command,
		arguments:     arguments,
		repositoryURL: repositoryURL,
		basepath:      config.Basepath,
		git:           config.GitConfig,
		logger:        config.Logger,
	}
}

func (a *runGitCommand) Execute(ctx context.Context) (string, error) {
	cmd, err := a.buildCmd(ctx, runtime.GOOS)
	if err != nil {
		return "", err
	}

	a.logger.Debug("runGitCommand: Running git command", zap.String("command", strings.Join(cmd.Args, " ")))
	// Create a GIT_ASKPASS command which echos the env var GIT_PASSWORD
	// and also set the GIT_PASSWORD env var. This is unfortunately kinda
	// hacky as there is no way to just set the password as an environment
	// variable.

	tempFile, err := os.CreateTemp("", "git-askpass")
	if err != nil {
		return "", err
	}

	defer os.Remove(tempFile.Name())

	// Make GIT_ASKPASS command executable
	err = os.Chmod(tempFile.Name(), 0o700)
	if err != nil {
		return "", err
	}

	_, err = tempFile.WriteString("#!/bin/sh\necho $GIT_PASSWORD")
	if err != nil {
		return "", err
	}
	tempFile.Close()

	cmd.Env = append(os.Environ(), "GIT_PASSWORD="+a.git.Password)
	cmd.Env = append(cmd.Env, "GIT_ASKPASS="+tempFile.Name())
	cmd.Env = append(cmd.Env, "GIT_CONFIG_NOSYSTEM=1")

	cmd.Dir = a.basepath

	output, err := cmd.CombinedOutput()
	if err != nil {
		a.logger.Debug("runGitCommand: Ran git command", zap.String("error", err.Error()))
		return "", err
	}
	a.logger.Debug("runGitCommand: Ran git command", zap.String("output", string(output)))

	processed := a.postProcess(string(output))

	return processed, nil
}

func extractBaseURL(url string) (string, error) {
	if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
		return "", fmt.Errorf("invalid URL: %s, scheme is missing. Supported scheme: http, https", url)
	}

	// Split the URL by '/'
	parts := strings.SplitN(url, "/", 4)
	if len(parts) >= 3 {
		return strings.Join(parts[:3], "/"), nil
	}

	return "", fmt.Errorf("invalid URL format: %s", url)
}

func (a *runGitCommand) buildCmd(ctx context.Context, goos string) (*exec.Cmd, error) {
	gArgs, err := a.defaultArgs()
	if err != nil {
		return nil, err
	}

	cArgs := ""
	if a.arguments != "" {
		cArgs += fmt.Sprintf(" %s", a.arguments)
	}

	cmdText := fmt.Sprintf("git %s %s %s", gArgs, a.command, cArgs)

	var cmd *exec.Cmd
	if goos == "windows" {
		cmd = exec.CommandContext(ctx, "cmd.exe", "/C", cmdText)
	} else {
		cmd = exec.CommandContext(ctx, "/bin/sh", "-c", cmdText)
	}

	return cmd, nil
}

func (a *runGitCommand) defaultArgs() (string, error) {
	args := make([]string, 0)

	gitBaseURL := a.git.HTTPBaseURL
	if a.git.HTTPBaseURL == "" {
		return "", fmt.Errorf("gitHTTPBaseURL is required")
	}

	baseURL, err := extractBaseURL(a.repositoryURL)
	if err != nil {
		// try to use default git url passed when executor is started
		baseURL, err = extractBaseURL(gitBaseURL)
		if err != nil {
			return "", err
		}
	}

	domain := strings.TrimPrefix(strings.TrimPrefix(baseURL, "http://"), "https://")

	if a.git.HTTPSUser != "" {
		args = append(args, fmt.Sprintf("-c credential.%s.username=%s", baseURL, a.git.HTTPSUser))
	}

	if a.git.IgnoreDirOwners {
		args = append(args, fmt.Sprintf("-c safe.directory=%s", a.basepath))
	}

	args = append(args, fmt.Sprintf("-c user.email=%s", a.git.UserEmail))
	args = append(args, fmt.Sprintf("-c url.%s/.insteadOf=git@%s:", baseURL, domain))

	return strings.Join(args, " "), nil
}

func (a *runGitCommand) postProcess(output string) string {
	if a.command != "grep" {
		return output
	}

	// post process grep output to remove long lines
	const maxLineLength = 5000

	lines := strings.Split(output, "\n")
	for i, line := range lines {
		if len(line) > maxLineLength {
			lines[i] = line[:maxLineLength] + "... (trimmed)"
		}
	}

	return strings.Join(lines, "\n")
}
