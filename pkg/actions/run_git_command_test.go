package actions

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/git"
)

func Test_runGitCommand_defaultArgs(t *testing.T) {
	type fields struct {
		gitHTTPBaseURL     string
		gitHTTPSUser       string
		gitUserEmail       string
		basePath           string
		ignoreGitDirOwners bool
		repositoryURL      string
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			name: "with HTTPS protocol",
			fields: fields{
				repositoryURL:      "https://gitlab.com",
				gitHTTPBaseURL:     "default.url",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: false,
			},
			want:    "-c credential.https://gitlab.com.username=auth -c user.email=test@example.com -c url.https://gitlab.com/.insteadOf=git@gitlab.com:",
			wantErr: false,
		},
		{
			name: "with HTTP protocol",
			fields: fields{
				repositoryURL:      "http://gitlab.com",
				gitHTTPBaseURL:     "default.url",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: false,
			},
			want: "-c credential.http://gitlab.com.username=auth -c user.email=test@example.com -c url.http://gitlab.com/.insteadOf=git@gitlab.com:",
		},
		{
			name: "with missing scheme",
			fields: fields{
				repositoryURL:      "gitlab.com",
				gitHTTPBaseURL:     "default.url",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: false,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "with safe directory",
			fields: fields{
				repositoryURL:      "https://gitlab.com",
				gitHTTPBaseURL:     "default.url",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: true,
			},
			want:    "-c credential.https://gitlab.com.username=auth -c safe.directory=/workspace -c user.email=test@example.com -c url.https://gitlab.com/.insteadOf=git@gitlab.com:",
			wantErr: false,
		},
		{
			name: "with port",
			fields: fields{
				repositoryURL:      "http://gdk.test:3000/duo-workflow-test/duo-workflow-test-project.git",
				gitHTTPBaseURL:     "default.url",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: true,
			},
			want:    "-c credential.http://gdk.test:3000.username=auth -c safe.directory=/workspace -c user.email=test@example.com -c url.http://gdk.test:3000/.insteadOf=git@gdk.test:3000:",
			wantErr: false,
		},
		{
			name: "with full URL",
			fields: fields{
				repositoryURL:      "https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-service.git",
				gitHTTPBaseURL:     "default.url",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: false,
			},
			want:    "-c credential.https://gitlab.com.username=auth -c user.email=test@example.com -c url.https://gitlab.com/.insteadOf=git@gitlab.com:",
			wantErr: false,
		},
		{
			name: "with broken repository URL passed from Duo Service",
			fields: fields{
				repositoryURL:      "",
				gitHTTPBaseURL:     "https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-service.git",
				gitHTTPSUser:       "auth",
				gitUserEmail:       "test@example.com",
				basePath:           "/workspace",
				ignoreGitDirOwners: false,
			},
			want:    "-c credential.https://gitlab.com.username=auth -c user.email=test@example.com -c url.https://gitlab.com/.insteadOf=git@gitlab.com:",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &runGitCommand{
				repositoryURL: tt.fields.repositoryURL,
				basepath:      tt.fields.basePath,
				logger:        zap.NewNop(),
				git: &git.Config{
					HTTPBaseURL:     tt.fields.gitHTTPBaseURL,
					HTTPSUser:       tt.fields.gitHTTPSUser,
					UserEmail:       tt.fields.gitUserEmail,
					IgnoreDirOwners: tt.fields.ignoreGitDirOwners,
				},
			}
			got, err := a.defaultArgs()
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func Test_runGitCommand_postProcess(t *testing.T) {
	tests := []struct {
		command  string
		name     string
		original string
		trimmed  string
	}{
		{
			name:     "normal line",
			command:  "status",
			original: "line1\nline2",
			trimmed:  "line1\nline2",
		},
		{
			name:     "long line",
			command:  "status",
			original: fmt.Sprintf("%s\nline2", strings.Repeat("a", 5100)),
			trimmed:  fmt.Sprintf("%s\nline2", strings.Repeat("a", 5100)),
		},
		{
			name:     "normal line",
			command:  "grep",
			original: "line1\nline2",
			trimmed:  "line1\nline2",
		},
		{
			name:     "long line",
			command:  "grep",
			original: fmt.Sprintf("%s\nline2", strings.Repeat("a", 5100)),
			trimmed:  fmt.Sprintf("%s... (trimmed)\nline2", strings.Repeat("a", 5000)),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &runGitCommand{
				command: tt.command,
			}
			got := a.postProcess(tt.original)
			require.Equal(t, tt.trimmed, got)
		})
	}
}

func Test_buildCmd(t *testing.T) {
	tests := []struct {
		goos            string
		expectedCommand []string
	}{
		{
			goos:            "windows",
			expectedCommand: []string{"cmd.exe", "/C"},
		},
		{
			goos:            "linux",
			expectedCommand: []string{"/bin/sh", "-c"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.goos, func(t *testing.T) {
			c := &runGitCommand{
				basepath:  "./workspace",
				command:   "commit",
				arguments: `-m "message"`,
				git: &git.Config{
					HTTPBaseURL:     "https://gitlab.com/gitlab-org/duo-workflow/duo-workflow-service.git",
					IgnoreDirOwners: true,
				},
			}

			cmd, err := c.buildCmd(t.Context(), tt.goos)
			require.NoError(t, err)

			require.Equal(t, tt.expectedCommand, cmd.Args[0:2])
			require.Equal(t, []string{`git -c safe.directory=./workspace -c user.email= -c url.https://gitlab.com/.insteadOf=git@gitlab.com: commit  -m "message"`}, cmd.Args[2:])
		})
	}
}
