package actions

import (
	"context"
	"fmt"
	"os"
)

type writeFile struct {
	fileName string
	contents string
	root     *os.Root
}

func NewWriteFile(fileName string, contents string, root *os.Root) *writeFile {
	return &writeFile{
		fileName: fileName,
		contents: contents,
		root:     root,
	}
}

func (a *writeFile) Execute(ctx context.Context) (string, error) {
	file, err := a.root.Create(a.fileName)
	if err != nil {
		return "", fmt.Errorf("unable to create file: %w", err)
	}
	defer file.Close()

	_, err = file.Write([]byte(a.contents))
	if err != nil {
		return "", fmt.Errorf("unable to write to file: %w", err)
	}

	return "File written successfully", nil
}
