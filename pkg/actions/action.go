package actions

import "context"

type Action interface {
	Execute(context.Context) (string, error)
}
