package actions_test

import (
	"context"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

func TestEditFile_Execute(t *testing.T) {
	// Create base directory for testing
	baseDir := t.TempDir()

	// Create root for base directory
	root, err := os.OpenRoot(baseDir)
	require.NoError(t, err)
	defer root.Close()

	tests := []struct {
		name              string
		initialContent    string
		oldString         string
		newString         string
		wantContent       string
		wantMessageFormat string
		wantErr           bool
		fileExists        bool
		fileName          string
	}{
		{
			name:              "replace existing string",
			initialContent:    "Hello World",
			oldString:         "World",
			newString:         "Go",
			wantContent:       "Hello Go",
			wantMessageFormat: "File '%s' has been updated",
			wantErr:           false,
			fileExists:        true,
			fileName:          "testfile.txt",
		},
		{
			name:              "old string not found",
			initialContent:    "Hello World",
			oldString:         "Python",
			newString:         "Go",
			wantContent:       "Hello World",
			wantMessageFormat: "No changes made to '%s' as '%s' not found.",
			wantErr:           false,
			fileExists:        true,
			fileName:          "testfile.txt",
		},
		{
			name:              "file does not exist",
			initialContent:    "",
			oldString:         "World",
			newString:         "Go",
			wantContent:       "",
			wantMessageFormat: "",
			wantErr:           true,
			fileExists:        false,
			fileName:          "nonexistent.txt",
		},
		{
			name:              "replace multiple occurrences",
			initialContent:    "World Hello World",
			oldString:         "World",
			newString:         "Go",
			wantContent:       "Go Hello Go",
			wantMessageFormat: "File '%s' has been updated",
			wantErr:           false,
			fileExists:        true,
			fileName:          "testfile.txt",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Setup: create the file with initial content if necessary
			if tt.fileExists {
				file, err := root.Create(tt.fileName)
				require.NoError(t, err, "Failed to create test file")
				_, err = file.Write([]byte(tt.initialContent))
				require.NoError(t, err, "Failed to write initial content")
				require.NoError(t, file.Close())
			}

			// Create the action
			ef := actions.NewEditFile(tt.fileName, tt.oldString, tt.newString, root)
			ctx := context.Background()
			result, err := ef.Execute(ctx)

			if tt.wantErr {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)

			// Compute expected message
			var expectedMessage string
			if tt.wantMessageFormat != "" {
				if tt.name == "old string not found" {
					expectedMessage = fmt.Sprintf(tt.wantMessageFormat, tt.fileName, tt.oldString)
				} else {
					expectedMessage = fmt.Sprintf(tt.wantMessageFormat, tt.fileName)
				}
				require.Equal(t, expectedMessage, result)
			}

			// Read the file content and verify
			file, err := root.Open(tt.fileName)
			require.NoError(t, err, "Failed to open file for reading")
			defer file.Close()

			content, err := io.ReadAll(file)
			require.NoError(t, err, "Failed to read file content")
			require.Equal(t, tt.wantContent, string(content), "File content mismatch")
		})
	}
}
