package actions

import (
	"context"
	"os/exec"
)

type runCommand struct {
	arguments []string
	basepath  string
	flags     []string
	program   string
}

func NewRunCommand(program string, basepath string, flags []string, arguments []string) *runCommand {
	return &runCommand{
		basepath:  basepath,
		arguments: arguments,
		flags:     flags,
		program:   program,
	}
}

func (a *runCommand) Execute(ctx context.Context) (string, error) {
	cmd := exec.CommandContext(ctx, a.program, append(a.flags, a.arguments...)...)
	cmd.Dir = a.basepath
	output, err := cmd.CombinedOutput()

	return string(output), err
}
