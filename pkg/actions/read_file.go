package actions

import (
	"context"
	"fmt"
	"io"
	"os"
)

type readfile struct {
	fileName string
	root     *os.Root
}

func NewReadFile(fileName string, root *os.Root) *readfile {
	return &readfile{
		fileName: fileName,
		root:     root,
	}
}

func (a *readfile) Execute(ctx context.Context) (string, error) {
	file, err := a.root.Open(a.fileName)
	if err != nil {
		return "", fmt.Errorf("unable to open file: %w", err)
	}
	defer file.Close()

	data, err := io.ReadAll(file)
	if err != nil {
		return "", fmt.Errorf("unable to read file: %w", err)
	}

	return string(data), nil
}
