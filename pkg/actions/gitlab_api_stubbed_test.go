package actions_test

import (
	"context"
	"errors"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

type mockAPI struct {
	response string
	path     string
	err      error
}

func (a *mockAPI) Execute(ctx context.Context) (string, error) {
	return a.response, a.err
}

func (a *mockAPI) URL() string {
	return "http://example.com" + a.path
}

func (a *mockAPI) MethodPath() string {
	return "GET_" + a.path
}

func TestGitLabAPIStubbed_Execute(t *testing.T) {
	tests := []struct {
		name        string
		path        string
		stubContent string
		apiContent  string
		record      bool
		want        string
		wantStored  string
		err         error
	}{
		{
			name:        "stub exists",
			path:        "/api/v4/projects/123",
			stubContent: "stubbed response",
			apiContent:  "real response",
			record:      true,
			want:        "stubbed response",
			wantStored:  "stubbed response",
		},
		{
			name:        "stub is missing and recording enabled",
			path:        "/api/v4/projects/123",
			stubContent: "",
			apiContent:  "real response",
			record:      true,
			want:        "real response",
			wantStored:  "real response",
		},
		{
			name:        "stub is missing and real api returns error",
			path:        "/api/v4/projects/123",
			stubContent: "",
			apiContent:  "real response",
			record:      true,
			want:        "real response",
			wantStored:  "",
			err:         errors.New("failure"),
		},
		{
			name:        "stub is missing and recording disabled",
			path:        "/api/v4/projects/123",
			stubContent: "",
			apiContent:  "real response",
			record:      false,
			want:        "real response",
			wantStored:  "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "example")
			if err != nil {
				log.Fatal(err)
			}
			defer os.RemoveAll(dir) // clean up

			api := actions.NewGitLabAPIStubbed(&mockAPI{response: tt.apiContent, path: tt.path, err: tt.err}, dir, tt.record)
			api.StoreResponse(tt.stubContent)
			ctx := context.Background()
			got, _ := api.Execute(ctx)
			assert.Equal(t, tt.want, got)
			stored := api.StubbedResponse()
			assert.Equal(t, tt.wantStored, stored)
		})
	}
}
