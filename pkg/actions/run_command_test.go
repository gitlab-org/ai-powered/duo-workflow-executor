package actions_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ai-powered/ai-framework/duo_workflow_executor/pkg/actions"
)

func TestRunCommand_Execute(t *testing.T) {
	tests := []struct {
		name      string
		program   string
		flags     []string
		arguments []string
		want      string
		wantErr   bool
	}{
		{
			name:      "successful command",
			program:   "echo",
			arguments: []string{"Hello!"},
			flags:     []string{},
			want:      "Hello!",
		},
		{
			name:      "command with flags",
			program:   "echo",
			flags:     []string{"-n"},
			arguments: []string{"Hello World"},
			want:      "Hello World",
		},
		{
			name:      "non-existent command",
			program:   "nonexistentcommand",
			arguments: []string{},
			flags:     []string{},
			wantErr:   true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := actions.NewRunCommand(tt.program, ".", tt.flags, tt.arguments)
			ctx := context.Background()
			got, err := rc.Execute(ctx)

			if tt.wantErr {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			require.Contains(t, got, tt.want)
		})
	}
}
