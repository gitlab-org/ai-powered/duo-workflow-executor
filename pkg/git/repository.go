package git

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/go-git/go-git/v5"
	"go.uber.org/zap"
)

type Repository struct {
	gitRepo *git.Repository
	logger  *zap.Logger
}

func NewRepository(basePath string, logger *zap.Logger) *Repository {
	repo, err := git.PlainOpen(basePath)
	if err != nil {
		logger.Warn("basePath is not a git repository", zap.String("basePath", basePath))
		return nil
	}

	return &Repository{
		gitRepo: repo,
		logger:  logger,
	}
}

func (repo *Repository) LastSHA() string {
	head, err := repo.gitRepo.Head()
	if err != nil {
		repo.logger.Warn("failed to get current git head", zap.String("error", err.Error()))
		return ""
	}

	return head.Hash().String()
}

// returns first remote URL defined in git config (typically for "origin")
func (repo *Repository) DefaultURL() string {
	var url string
	config, err := repo.gitRepo.Config()
	if err != nil {
		repo.logger.Warn("failed to get git config")
		return url
	}

	for _, cfg := range config.Remotes {
		if len(cfg.URLs) == 0 {
			continue
		}
		url = SanitizeURL(cfg.URLs[0])
		break
	}

	return url
}

func SanitizeURL(rawURL string) string {
	parsedURL, err := url.Parse(rawURL)
	if err == nil {
		return fmt.Sprintf("%s%s", parsedURL.Hostname(), parsedURL.Path)
	}

	if strings.HasPrefix(rawURL, "git@") {
		return rawURL
	}

	return "unparseable-url"
}
