package git

import (
	"os/exec"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
)

func setupRepo(t *testing.T) string {
	t.Helper()
	dir := t.TempDir()
	// go's git doesn't seem to support cloning bundles
	cmd := exec.Command("git", "clone", "-b", "master", "testdata/test_repo.bundle", dir)
	assert.Nil(t, cmd.Run())
	return dir
}

func TestNewRepository(t *testing.T) {
	assert.Nil(t, NewRepository("notexisting", zaptest.NewLogger(t)))

	dir := setupRepo(t)
	assert.NotNil(t, NewRepository(dir, zaptest.NewLogger(t)))
}

func TestRepository_LastSHA(t *testing.T) {
	dir := setupRepo(t)
	repo := NewRepository(dir, zaptest.NewLogger(t))
	assert.Equal(t, "b9f96f158c14892609e800875d2bf689c525218a", repo.LastSHA())
}

func TestRepository_DefaultURL(t *testing.T) {
	dir := setupRepo(t)
	repo := NewRepository(dir, zaptest.NewLogger(t))
	assert.True(t, strings.HasSuffix(repo.DefaultURL(), "/pkg/git/testdata/test_repo.bundle"))
}

func TestSanitizeUrl(t *testing.T) {
	urlTests := []struct {
		url      string
		expected string
	}{
		{"git@gitlab.com:gitlab-org/duo-workflow/duo-workflow-executor.git", "git@gitlab.com:gitlab-org/duo-workflow/duo-workflow-executor.git"},
		{"https://gitlab.com/gitlab-org/cli.git", "gitlab.com/gitlab-org/cli.git"},
		{"https://user@gitlab.com/gitlab-org/cli.git", "gitlab.com/gitlab-org/cli.git"},
		{"https://user:password@gitlab.com/gitlab-org/cli.git", "gitlab.com/gitlab-org/cli.git"},
		{"ssh://user@gitlab.com/gitlab-org/cli.git", "gitlab.com/gitlab-org/cli.git"},
		{"git://user@gitlab.com/gitlab-org/cli.git", "gitlab.com/gitlab-org/cli.git"},
		{"ftps://user@gitlab.com/gitlab-org/cli.git", "gitlab.com/gitlab-org/cli.git"},
	}

	for _, tt := range urlTests {
		assert.Equal(t, tt.expected, SanitizeURL(tt.url))
	}
}
