package git

type Config struct {
	HTTPSUser       string
	HTTPBaseURL     string
	Password        string
	UserEmail       string
	IgnoreDirOwners bool
}
