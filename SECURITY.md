# Security Policy

## Reporting a Vulnerability

Please follow the processes in https://about.gitlab.com/security/disclosure/ to report a security vulnerability.
